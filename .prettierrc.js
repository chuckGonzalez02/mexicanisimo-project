module.exports = {
  jsxBracketSameLine: false,
  trailingComma: 'all',
  bracketSpacing: true,
  jsxSingleQuote: false,
  arrowParens: 'always',
  singleQuote: true,
  printWidth: 100,
  useTabs: false,
  semi: true,
};
