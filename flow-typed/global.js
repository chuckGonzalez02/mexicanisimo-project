declare module CSSModule {
  declare export default Object;
}

declare module SVGModule {
  declare export var ReactComponent: JSX.Element;
}

declare module Audio {
  declare export default string;
}
