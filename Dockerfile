FROM wernight/alpine-nginx-pagespeed
COPY build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
ENV PORT=80
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
