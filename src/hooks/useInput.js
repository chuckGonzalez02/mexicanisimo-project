// @flow

import React from 'react';

type ValidationInputEvent = 'change' | 'blur';

type Replacer<value> = (value) => string;

type CustomValidation<value> = (value) => boolean;

export type useInputErrors = {
  requiredError?: string,
  defaultError?: string,
  minlengthError?: string,
};

export type useInputProps = {|
  name: string,
  value?: string,
  errors?: useInputErrors,
  type?: string,
  spellCheck?: boolean,
  required?: boolean,
  regexp?: string | RegExp,
  regexpOverwrite?: string | RegExp,
  placeholder?: string,
  validateEvent?: ValidationInputEvent,
  toLowerCase?: boolean,
  toUpperCase?: boolean,
  replacer?: Replacer<string>,
  minlength?: number,
  maxlength?: number,
  customValidation?: CustomValidation<string>,
  disabled?: boolean,
  validateMinLengthOnBlur?: boolean,
  maskRegex?: string | RegExp,
  maskPermanents?: Array<number>,
  maskPermanentChar?: string,
  maskChar?: string,
  maskPrefill?: boolean,
|};

export type inputRef = {|
  displayValue: any | string,
  id: string,
  input: {|
    disabled: boolean,
    error: any | string,
    onBlur: (e: Event) => void,
    onChange: (e: any) => void,
    placeholder: string,
    spellCheck: boolean,
    type: string,
    value: any | string,
  |},
  name: string,
|};

export type useInputRef = {|
  input: inputRef,
  setData: (((any) => any) | any) => void,
  type: string,
  validate: (config?: any) => boolean,
|};

export default function useInput({
  // Nombre del campo en el formulario
  name,
  // Valor inicial
  value,
  // Objeto de errores a mostrar incluye required, default y min, ver + abajo
  errors = {},
  // Tipo del input
  type = 'text',
  // Activar o desactivar correciones ortograficas
  spellCheck = false,
  // Marcar como requerido
  required = false,
  // Expresion para activar errores
  regexp = null,
  // Expresion para remplazar
  regexpOverwrite = null,
  placeholder = '',
  // Evento en cual validar ya sea change o blur
  validateEvent = '',
  // Mandar todo a minuscula
  toLowerCase = false,
  // Mandar todo a mayuscula
  toUpperCase = false,
  /**
   * Funcion para hacer un replace custom ej:
   * (valor) => { return nuevoValor }
   */
  replacer = null,
  minlength = null,
  maxlength = null,
  /**
   * Funcion para hacer un validacion personalizada:
   * (valor) => { return esValido; }
   */
  customValidation = null,
  // Desactivar interaccion en el input
  disabled = false,
  // Forzar validacion de longitud en blur
  validateMinLengthOnBlur = false,
  // Regex para generar mascara, recordar que reemplazara solo los grupos de captura
  maskRegex = null,
  // Array de indices de la cadena donde se tiene que mantener caracter fijo
  maskPermanents = null,
  // Cartacter fijo a mostrar en las mascaras ej "/" en "23/02/1991"
  maskPermanentChar = ' ',
  // Caracter que reemplazara los grupos de captura
  maskChar = '*',
  // activar prefill de los permanentes
  maskPrefill = false,
}: useInputProps): useInputRef {
  const [data, setData] = React.useState({
    value: value || '',
    displayValue: value || '',
    error: '',
  });

  const { requiredError = '', defaultError = '', minlengthError = '' } = errors;

  const validate = (config: any = {}) => {
    const { avoidValidation = false } = config;
    const { value: inputValue } = data;
    let error = '';
    if (required && !inputValue) error = requiredError;
    else if (minlength && minlength > inputValue.length && !!inputValue.length)
      error = minlengthError;
    else if (regexp && inputValue) {
      if (!new RegExp(regexp).test(inputValue)) error = defaultError;
    }
    if (customValidation) {
      if (!customValidation(inputValue)) error = defaultError;
    }
    if (!avoidValidation) setData((currentState) => ({ ...currentState, error }));
    return !!error;
  };

  const input = {
    disabled,
    spellCheck,
    placeholder,
    type: type || 'text',
    onChange: (e) => {
      let { value: targetValue } = e.target;
      let error = '';
      if (toLowerCase) targetValue = targetValue.toLowerCase();
      else if (toUpperCase) targetValue = targetValue.toUpperCase();

      if (maxlength) targetValue = targetValue.substring(0, maxlength);

      if (validateEvent === 'change') {
        if (required && !targetValue) error = requiredError;
        else if (!validateMinLengthOnBlur && minlength > targetValue.length) error = minlengthError;
        else if (regexp && targetValue) {
          if (!new RegExp(regexp).test(targetValue)) error = defaultError;
        }
        if (customValidation) {
          if (!customValidation(targetValue)) error = defaultError;
        }
      }

      if (regexpOverwrite) {
        targetValue = (targetValue.match(new RegExp(regexpOverwrite)) || []).join('');
      }

      let displayValue = targetValue;

      if (maskPermanents) {
        maskPermanents.forEach((indexValue) => {
          if (displayValue.length === indexValue) displayValue += maskPermanentChar;
          else if (displayValue.length > indexValue) {
            displayValue =
              displayValue.substring(0, indexValue) +
              maskPermanentChar +
              displayValue.substring(indexValue);
          }
        });
      }

      if (maskRegex) {
        displayValue.replace(maskRegex, (...args) => {
          args.forEach((groupValue, index) => {
            if (groupValue && index > 0 && index < args.length - 2) {
              displayValue = displayValue.replace(groupValue, maskChar.repeat(groupValue.length));
            }
          });
        });
      }

      if (maskPermanents && maskPrefill) {
        const lastPermanent = maskPermanents[maskPermanents.length - 1];
        if (displayValue.length < lastPermanent) displayValue.padEnd(lastPermanent, ' ');
      }

      if (maskPermanents) {
        maskPermanents.forEach((indexValue) => {
          if (displayValue[indexValue]) {
            displayValue =
              displayValue.substring(0, indexValue) +
              maskPermanentChar +
              displayValue.substring(indexValue + 1);
          }
        });
      }

      if (replacer) targetValue = replacer(targetValue);

      setData({ value: targetValue, displayValue, error });
    },
    onBlur: (e: Event) => {
      const { value: targetValue } = e.target;
      let error = '';

      if (validateEvent === 'blur') {
        if (required && !targetValue) error = requiredError;
        else if (minlength > targetValue.length) error = minlengthError;
        else if (regexp && targetValue) {
          if (!new RegExp(regexp).test(targetValue)) error = defaultError;
        }
        if (customValidation) {
          if (!customValidation(targetValue)) error = defaultError;
        }
      } else if (validateMinLengthOnBlur) {
        if (minlength > targetValue.length) error = minlengthError;
      }

      setData((state) => ({ ...state, value: targetValue, error }));
    },
    value: data.value,
    error: data.error,
  };

  return {
    input: { input, displayValue: data.displayValue, id: name, name },
    setData,
    validate,
    type,
  };
}
