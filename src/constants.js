// Eventos
export const ENTER = 'Enter';
export const ESCAPE = 'Escape';
export const ARROW_DOWN = 'ArrowDown';
export const ARROW_UP = 'ArrowUp';

// Localstorage
export const STORAGE_SELECTED_ADDRESS_ID = 'selectedAddressId';
export const STORAGE_COUPON = 'coupon';

// Estilos
export const PRESSED_SCALE = 'scale(0.9)';
export const UNPRESSED_SCALE = 'scale(1)';
export const OPAQUE_STYLE = { opacity: 1 };
export const TRANSPARENT_STYLE = { opacity: 0 };

// Inputs
export const SELECT_NON_VALUE = '0';

// Urls
export const SPOTIFY_PLAYLIST =
  'https://open.spotify.com/playlist/7H2ZpBIzr86MVvGq9gpPQm?si=MwnZTwO5TFSXDVn1O6ZbXw';

// Keys
export const GMAPS_KEY = 'AIzaSyDO4vjn5Y8_FgyxmNMdKCqb50YYFRFA8eU';

// Fetch policy
export const NETWORK_ONLY = 'network-only';

// Spring
export const PRESS_CONFIG = {
  mass: 1,
  tension: 0,
  friction: 0,
  velocity: 0,
};
