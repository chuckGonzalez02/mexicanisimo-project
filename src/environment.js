// Environment https://relay.dev/docs/en/network-layer.html
import { Store, Network, Environment, RecordSource, QueryResponseCache } from 'relay-runtime';
import persistedQueries from './persisted-queries.json';

/**
 * Cache graphql responses into relay data
 * 5 minutes to cache responses from graphql
 * https://relay.dev/docs/en/network-layer.html#caching
 */
const ttl = 60000 * 5;
const cache = new QueryResponseCache({ size: 250, ttl });
const endpoint = '/_graphql?db=mexicanisimo&sessionMode=identity';

// Function to fetch from grapqhl
function fetcher({ isQuery, isMutation, operation, variables }) {
  // We send the query(operation.text) and the variables of the query
  const body = {
    variables,
    query: operation.text || persistedQueries[operation.id],
  };

  return (
    fetch(endpoint, {
      mode: 'cors',
      credentials: 'omit',
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
        /**
         * We read localstorage on every runtime because token can mutate
         * in the life of the session
         */
        Authorization: `Bearer ${localStorage.token || ''}`,
      },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      /** .then((json) => {
      return new Promise((resolve) => {
        setTimeout(() => resolve(json), 5000);
      });
    })
    */
      .then((json) => {
        if (json?.data?.viewer?.token) localStorage.setItem('token', json.data.viewer.token);
        /**
         * If the operation is a query we need to cache the result for that query
         * and variables. But in mutation we are changing the data
         * so we clear the cache
         */
        if (isQuery && json) cache.set(operation.text || operation.id, variables, json);
        if (isMutation) cache.clear();
        return json;
      })
      .catch((err) => {
        // eslint-disable-next-line no-console
        console.trace(err);
      })
  );
}

function fetchQuery(operation, variables /* , cacheConfig */) {
  const isMutation = operation.operationKind === 'mutation';
  const isQuery = operation.operationKind === 'query';

  /**
   * We receive this flag from relay, if cacheConfig.force is true
   * we ignore the cache and execute fetcher helper.
   *
   * We lookup for data in the cache with the query(operation.text) and variables
   */
  // const forceFetch = cacheConfig && cacheConfig.force;
  const fromCache = cache.get(operation.text || operation.id, variables);

  if (isQuery && fromCache !== null) {
    return new Promise((resolve) => resolve(fromCache));
  }

  // Return fetcher promise
  return fetcher({ isQuery, isMutation, operation, variables });
}

// Function helper to import graphql fragments for modules
const getOperation = (reference) => {
  return import(/* webpackChunkName: "[request]" */ `./__generated__/${reference}`);
};

/**
 * Helper function to get graphql fragments for modules
 * It creates a cache so getOperation imports only runs the first time
 * After that the fragment comes from cache object
 */
function createOperationLoader() {
  const operationCache = new Map();

  const loader = {
    get: (moduleName) => {
      const entry = operationCache.get(moduleName);
      if (entry && entry.kind === 'value') {
        return entry.operation;
      }
      return null;
    },
    load: (moduleName) => {
      let entry = operationCache.get(moduleName);
      if (entry == null) {
        const promise = getOperation(moduleName);
        entry = { kind: 'promise', promise };
        operationCache.set(moduleName, entry);
        return promise;
      }
      if (entry.kind === 'value') {
        return Promise.resolve(entry.operation);
      }
      return entry.promise;
    },
  };
  return loader;
}

const operationLoader = createOperationLoader();
const store = new Store(new RecordSource(), undefined, operationLoader);
const network = Network.create(fetchQuery);
const environment = new Environment({
  store,
  network,
  operationLoader,
});

export default environment;
