// @flow strict

// Librerias
import React from 'react';
import useDevice from 'usedevice';
import { BrowserRouter as Router, Route, useRouteMatch } from 'react-router-dom';
import { graphql, useQueryLoader, RelayEnvironmentProvider } from 'react-relay/hooks';

// Componentes
import Loader from './components/Loader';

// Estilos
import styles from './App.atomic.sass';

// React Relay env
import RelayEnvironment from './environment';

// Constantes
import { STORAGE_COUPON } from './constants';

const coupon = sessionStorage.getItem(STORAGE_COUPON);

// Views
const Sign = React.lazy(() => import('./views/Sign'));
const Address = React.lazy(() => import('./views/Address'));
const Content = React.lazy(() => import('./views/Content'));
const Spotify = React.lazy(() => import('./views/Spotify'));
const Checkout = React.lazy(() => import('./views/Checkout'));
const Confirmation = React.lazy(() => import('./views/Confirmation'));

// Queries
const sessionQuery = graphql`
  query App_Session_Query {
    viewer {
      id
      token
      ...Session

      cart {
        ...Cart
      }
    }
  }
`;

const contentQuery = graphql`
  query App_Content_Query($defaultFilter: Filter) {
    taxonomy {
      ...Menu
    }
    ...Stories
  }
`;

const addressesQuery = graphql`
  query App_Address_Query($address: AddressInput) {
    viewer {
      id
      address(address: $address) {
        id
        externalNumber
        internalNumber
        neighborhood
        postalCode
        reference
        street
        place
        geopoint {
          lat
          lng
        }
      }
      addresses {
        id
        externalNumber
        internalNumber
        neighborhood
        postalCode
        reference
        street
        place
        geopoint {
          lat
          lng
        }
      }
    }
  }
`;

const checkoutQuery = graphql`
  query App_Checkout_Query($address: String, $couponRequested: String) {
    viewer {
      id
      name
      lastName
      phone
      email
      checkout(address: $address, couponRequested: $couponRequested) {
        id
        isEmpty
        cart {
          id
          items {
            id
            qty
            total
            data {
              name
            }
          }
          subtotal
          delivery
          sales
          total
        }
        address {
          id
          externalNumber
          internalNumber
          neighborhood
          postalCode
          reference
          street
          selected
          place
          geopoint {
            lat
            lng
          }
        }
        addresses {
          id
          externalNumber
          internalNumber
          neighborhood
          postalCode
          reference
          street
          selected
          place
          geopoint {
            lat
            lng
          }
        }
        deliveryMetadata {
          durationInMinutes
          standardFee
          extraKilometersFee
        }
      }
    }
  }
`;

const confirmationQuery = graphql`
  query App_Confirmation_Query($order: String) {
    viewer {
      id
      order(order: $order) {
        id
        isEmpty
        status
        chargeDescription
        cart {
          id
          items {
            id
            qty
            total
            data {
              name
            }
          }
          subtotal
          delivery
          sales
          total
        }
        address {
          id
          externalNumber
          internalNumber
          neighborhood
          postalCode
          reference
          street
          geopoint {
            lat
            lng
          }
        }
        transaction {
          id
          status
          success
          orderID
          status_detail
          date_created
        }
      }
    }
  }
`;

function DefaultRouteInterface() {
  const [contentQueryReference, loadContentQuery] = useQueryLoader(contentQuery);
  const [sessionQueryReference, loadSessionQuery] = useQueryLoader(sessionQuery);
  const [addressQueryReference, loadAddressQuery] = useQueryLoader(addressesQuery);

  React.useEffect(() => {
    loadContentQuery({
      defaultFilter: {
        name: 'scategory.keyword',
        value: 'garnacheria',
      },
    });

    loadSessionQuery({});

    loadAddressQuery({
      address: {},
    });
  }, [loadContentQuery, loadSessionQuery, loadAddressQuery]);

  return contentQueryReference && sessionQueryReference ? (
    <React.Suspense fallback={<Loader />}>
      <Content
        addressesQuery={addressesQuery}
        loadAddressQuery={loadAddressQuery}
        addressQueryReference={addressQueryReference}
        contentQueryReference={contentQueryReference}
        sessionQueryReference={sessionQueryReference}
      />
    </React.Suspense>
  ) : null;
}

function CheckoutInterface() {
  const [queryReference, loadQuery] = useQueryLoader(checkoutQuery);

  React.useEffect(() => {
    loadQuery({ couponRequested: coupon || null });
  }, [loadQuery]);

  return queryReference ? (
    <React.Suspense fallback={<Loader />}>
      <Checkout preloadedQuery={queryReference} loadQuery={loadQuery} />
    </React.Suspense>
  ) : null;
}

function ConfirmationInterface() {
  const match = useRouteMatch('/confirmacion/:order');
  const { order } = match.params;

  const [queryReference, loadQuery] = useQueryLoader(confirmationQuery);

  React.useEffect(() => {
    loadQuery({ order });
  }, [loadQuery, order]);

  return (
    <React.Suspense fallback={<Loader />}>
      <Confirmation query={confirmationQuery} queryReference={queryReference} />
    </React.Suspense>
  );
}

export default function App() {
  const device = useDevice();

  return (
    <section className={styles.app}>
      <RelayEnvironmentProvider environment={RelayEnvironment}>
        <Router>
          <Route path="/spotify">
            <React.Suspense fallback={<Loader />}>
              <Spotify device={device} />
            </React.Suspense>
          </Route>
          <Route path="/(registro|iniciar-sesion)">
            <React.Suspense fallback={<Loader />}>
              <Sign />
            </React.Suspense>
          </Route>
          <Route path="/direcciones">
            <React.Suspense fallback={<Loader />}>
              <Address />
            </React.Suspense>
          </Route>
          <Route path="/" exact>
            <DefaultRouteInterface />
          </Route>
          <Route path="/checkout/:step?" exact>
            <CheckoutInterface />
          </Route>
          <Route path="/confirmacion/:order" exact>
            <ConfirmationInterface />
          </Route>
        </Router>
      </RelayEnvironmentProvider>
    </section>
  );
}
