// @flow

// Librerias
import * as React from 'react';
import { useSpring, animated } from 'react-spring';

// Constantes
import { ENTER, ESCAPE, PRESSED_SCALE, UNPRESSED_SCALE, PRESS_CONFIG } from '../../constants';

// Flow
type Props = {
  onClick: Function,
  className?: string,
  children: React.Node,
  avoidAnimation?: boolean,
};

export default function InteractiveElement({
  onClick,
  children,
  className = '',
  avoidAnimation,
}: Props) {
  const [pressed, setPressed] = React.useState(false);

  function updateOnKey(e): void {
    const { key } = e;
    if (key === ENTER) onClick();
    else if (key === ESCAPE) e.target.blur();
  }

  function handleClick() {
    onClick();
  }

  function handleMouseDown(e): void {
    e.preventDefault();
    if (!avoidAnimation) setPressed(true);
  }

  function handleMouseUp(): void {
    if (!avoidAnimation) setPressed(false);
  }

  function handleMouseLeave(): void {
    if (!avoidAnimation) setPressed(false);
  }

  const animatedStyles = useSpring({
    config: PRESS_CONFIG,
    transform: pressed ? PRESSED_SCALE : UNPRESSED_SCALE,
  });

  return (
    <animated.div
      tabIndex="0"
      role="button"
      onClick={handleClick}
      className={className}
      style={animatedStyles}
      onKeyDown={updateOnKey}
      onMouseUp={handleMouseUp}
      onMouseDown={handleMouseDown}
      onMouseLeave={handleMouseLeave}
    >
      {children}
    </animated.div>
  );
}

InteractiveElement.defaultProps = {
  className: '',
  avoidAnimation: false,
};
