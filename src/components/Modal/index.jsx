// @flow

// Librerias
import * as React from 'react';
import { useHistory } from 'react-router-dom';

// Estilos
import styles from './Modal.atomic.sass';

// Componentes
import Tap from '../Tap';

// Iconos
import { ReactComponent as LogoIcon } from '../Icons/logo.svg';

type Props = {
  children: React.Node,
};

export default function Modal({ children }: Props) {
  const history = useHistory();

  function goToHome() {
    history.push('/');
  }

  function goBack() {
    history.goBack();
  }

  return (
    <div className={styles.modal}>
      <div className={styles.toolbar}>
        <Tap icon="arrow_back" className={styles.closeTap} label="Regresar" onClick={goBack} />
        <Tap label="Inicio" onClick={goToHome}>
          <div className={styles.logoContainer}>
            <LogoIcon className={styles.logo} />
          </div>
        </Tap>
      </div>
      <div className={styles.overlay}>
        <div className={styles.content}>{children}</div>
      </div>
    </div>
  );
}
