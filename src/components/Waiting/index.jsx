// @flow

// Librerias
import React from 'react';

// Componentes
import Lottie from '../Lottie';
import animationData from './data.json';

const defaultOptions = {
  animationData,
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

type Props = {
  width?: number,
  height?: number,
};

export default function Waiting({ width = 38, height = 38 }: Props) {
  const remSize = parseInt(window.getComputedStyle(document.documentElement).fontSize, 10);

  return (
    <Lottie
      options={defaultOptions}
      height={height * remSize}
      width={width * remSize}
      isStopped={false}
      isPaused={false}
    />
  );
}

Waiting.defaultProps = {
  width: 2,
  height: 2,
};
