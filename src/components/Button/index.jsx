// @flow

// Librerias
import * as React from 'react';

// Estilos
import styles from './Button.atomic.sass';

// Flow
type Props = {|
  inline?: boolean,
  onClick: Function,
  className?: string,
  children: React.Node,
|};

export default function Button({ inline, children, onClick, className = '' }: Props) {
  function preventDefault(e) {
    e.preventDefault();
  }

  function handleClick(e) {
    onClick(e);
  }

  return (
    <button
      type="button"
      className={`${inline ? styles.inlineButton : styles.button} ${className}`}
      onClick={handleClick}
      onMouseDown={preventDefault}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  inline: false,
  className: '',
};
