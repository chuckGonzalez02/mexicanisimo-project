// @flow

// Librerias
import React from 'react';
import { useHistory } from 'react-router-dom';
import { graphql, usePreloadedQuery, useFragment } from 'react-relay/hooks';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Artifacts
import AppQuery from '../../__generated__/App_Session_Query.graphql';

// Componentes
import Button from '../Button';

// Estilos
import styles from './Session.atomic.sass';

const fragment = graphql`
  fragment Session on Viewer {
    id
    name
    lastName
  }
`;

type Props = {
  preloadedQuery: preloadQueryType,
};

export default function Session({ preloadedQuery }: Props) {
  const data = usePreloadedQuery(AppQuery, preloadedQuery);
  const sessionData = useFragment(fragment, data.viewer);
  const { name } = sessionData;
  const isLoggedIn = !!name;

  const history = useHistory();

  function showRegister() {
    history.push('/registro');
  }

  function showLogin() {
    history.push('/iniciar-sesion');
  }

  function logout() {
    localStorage.removeItem('token');
    window.location.href = '/';
  }

  return (
    <div className={styles.content}>
      <h3 className={styles.title}>{isLoggedIn ? `¡Hola ${name}!` : '¡Aún no nos conocemos!'}</h3>
      {!isLoggedIn && (
        <>
          <p className={styles.text}>
            Nos gustaría conocerte, regístrate para mejorar tu experiencia de compra y enterate de
            nuestras promociones
          </p>
          <Button onClick={showRegister}>
            <span>Crea una cuenta</span>
          </Button>
          <div className={styles.separatorContainer}>
            <strong className={styles.separator}>ó</strong>
          </div>
          <Button inline onClick={showLogin}>
            <span>Inicia sesión</span>
          </Button>
        </>
      )}
      {isLoggedIn && (
        <>
          <Button inline onClick={logout}>
            <span>Cerrar sesión</span>
          </Button>
        </>
      )}
    </div>
  );
}
