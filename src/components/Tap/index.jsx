// @flow

// Librerias
import * as React from 'react';
import { useSpring, animated, useTransition } from 'react-spring';

// Estilos
import styles from './Tap.atomic.sass';

// Constantes
import {
  ENTER,
  ESCAPE,
  PRESSED_SCALE,
  UNPRESSED_SCALE,
  PRESS_CONFIG,
  OPAQUE_STYLE,
  TRANSPARENT_STYLE,
} from '../../constants';

// Flow
type IconProps = {
  size?: string,
  icon?: string,
  label?: string,
  active?: boolean,
  className?: string,
  onClick?: Function,
  children?: React.Node,
  elementClassName?: string,
};

// Constantes
const DEFAULT = 'default';

export default function Tap({
  label,
  icon,
  active,
  onClick,
  children,
  className,

  size = DEFAULT,
  elementClassName = '',
}: IconProps) {
  const [pressed, setPressed] = React.useState(false);
  const [labelVisibility, setLabelVisibility] = React.useState(false);

  const sizeIsDefault = size === DEFAULT;

  const activeClassName = active ? styles.active : styles.inactive;
  const containerClassName = sizeIsDefault ? styles.container : styles.miniContainer;
  const elementSizeClassName = sizeIsDefault ? styles.element : styles.miniElement;
  const iconClassName = sizeIsDefault ? styles.icon : styles.miniIcon;
  const labelClassName = sizeIsDefault ? styles.label : styles.miniLabel;
  const labelContentClassName = sizeIsDefault ? styles.labelContent : styles.miniLabelContent;

  function handleMouseEnter() {
    setLabelVisibility(true);
  }

  function handleMouseLeave() {
    setLabelVisibility(false);
    setPressed(false);
  }

  function handleMouseDown(e): void {
    e.preventDefault();
    setPressed(true);
  }

  function handleMouseUp(): void {
    setPressed(false);
    setLabelVisibility(false);
  }

  function handleKeyDown(e) {
    const { key } = e;
    if (key === ENTER && onClick) onClick();
    else if (key === ESCAPE) e.target.blur();
  }

  function handleClick() {
    if (onClick) onClick();
  }

  const transitions = useTransition(labelVisibility, null, {
    from: TRANSPARENT_STYLE,
    enter: OPAQUE_STYLE,
    leave: TRANSPARENT_STYLE,
  });

  const animatedStyles = useSpring({
    config: PRESS_CONFIG,
    transform: pressed ? PRESSED_SCALE : UNPRESSED_SCALE,
  });

  return (
    <div className={`${containerClassName} ${className || ''}`}>
      <animated.button
        type="button"
        onClick={handleClick}
        style={animatedStyles}
        onMouseUp={handleMouseUp}
        onKeyPress={handleKeyDown}
        onMouseDown={handleMouseDown}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        className={`${styles.elementBase} ${elementSizeClassName} ${elementClassName} ${activeClassName}`}
      >
        {Boolean(icon) && <i className={`material-icons-outlined ${iconClassName}`}>{icon}</i>}
        {Boolean(children) && children}
      </animated.button>
      {!!label &&
        transitions.map(
          ({ item, key, props }) =>
            item && (
              <animated.div
                key={key}
                style={props}
                className={`${styles.labelBase} ${labelClassName}`}
              >
                <span className={`${styles.labelContentBase} ${labelContentClassName}`}>
                  {label}
                </span>
              </animated.div>
            ),
        )}
    </div>
  );
}

Tap.defaultProps = {
  icon: '',
  label: '',
  active: false,
  className: '',
  size: DEFAULT,
  children: null,
  onClick: undefined,
  elementClassName: '',
};
