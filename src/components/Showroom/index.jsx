// @flow

// Librerias
import React from 'react';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componentes
import Stories from '../Stories';

// Estilos
import styles from './Showroom.atomic.sass';

// Flow
type Props = {
  contentQueryReference: preloadQueryType,
  sessionQueryReference: preloadQueryType,
};

function StoriesLoader() {
  return (
    <>
      <div className={styles.loaderContainer}>
        <div className={styles.header}>
          <div className={styles.thumb} />
          <div className={styles.headerLabel} />
        </div>
        <div className={styles.storyPlaceholder} />
      </div>
      <div className={styles.menu}>
        <div className={styles.menuItem} />
        <div className={styles.menuItem} />
        <div className={styles.menuItem} />
        <div className={styles.menuItem} />
      </div>
    </>
  );
}

export default function Showroom({ sessionQueryReference, contentQueryReference }: Props) {
  return (
    <section className={styles.content}>
      <div className={styles.stories}>
        <React.Suspense fallback={<StoriesLoader />}>
          <Stories
            sessionQueryReference={sessionQueryReference}
            contentQueryReference={contentQueryReference}
          />
        </React.Suspense>
      </div>
    </section>
  );
}
