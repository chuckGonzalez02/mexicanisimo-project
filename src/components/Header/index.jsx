// @flow

// Librerias
import React, { Suspense } from 'react';
import { Link } from 'react-router-dom';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componentes
import Tap from '../Tap';
import Cart from '../Cart';
import AddressSelector from '../AddressSelector';

// Constantes
import { NETWORK_ONLY } from '../../constants';

// Estilos
import styles from './Header.atomic.sass';

// Iconos
import { ReactComponent as LogoIcon } from '../Icons/logo.svg';
import { ReactComponent as TypoIcon } from '../Icons/typo.svg';

// Componentes
const Session = React.lazy(() => import('../Session'));

// Tools data
const tools = [
  {
    icon: 'shopping_basket',
    label: 'Canasta',
  },
  {
    icon: 'notifications',
    label: 'Notificaciones',
  },
  {
    icon: 'person',
    label: 'Cuenta',
  },
];

// Flow
type Props = {|
  addressesQuery: any,
  loadAddressQuery: Function,
  addressQueryReference: preloadQueryType,
  sessionQueryReference: preloadQueryType,
|};

export default function Header({
  addressesQuery,
  loadAddressQuery,
  addressQueryReference,
  sessionQueryReference,
}: Props) {
  const [selectedTool, setSelectedTool] = React.useState(0);

  function handleAddressChange(id: string): void {
    loadAddressQuery(
      { address: id },
      {
        fetchPolicy: NETWORK_ONLY,
      },
    );
  }

  return (
    <React.Suspense fallback={<div className={styles.headerLoader} />}>
      <header className={styles.container}>
        <Link to="/">
          <div className={styles.main}>
            <LogoIcon className={styles.logoIcon} />
            <TypoIcon className={styles.typoIcon} />
          </div>
        </Link>
        <div className={styles.tools}>
          {tools.map(({ icon, label }, index) => {
            function selectItem() {
              setSelectedTool(index);
            }
            return (
              <Tap
                key={icon}
                icon={icon}
                label={label}
                onClick={selectItem}
                className={styles.tap}
                active={selectedTool === index}
              />
            );
          })}
        </div>
        <div className={styles.view}>
          {selectedTool === 0 && (
            <>
              <div className={styles.addressContainer}>
                <AddressSelector
                  query={addressesQuery}
                  onChange={handleAddressChange}
                  queryReference={addressQueryReference}
                />
              </div>
              <Cart preloadedQuery={sessionQueryReference} />
            </>
          )}
          {selectedTool === 2 && (
            <Suspense fallback={<div />}>
              <Session preloadedQuery={sessionQueryReference} />
            </Suspense>
          )}
        </div>
      </header>
    </React.Suspense>
  );
}
