// @flow

// Librerias
import React from 'react';
import { graphql, usePreloadedQuery, useFragment } from 'react-relay/hooks';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componenets
import MenuItem from '../MenuItem';

// Artifacts
import AppQuery from '../../__generated__/App_Content_Query.graphql';

// Estilos
import styles from './Menu.atomic.sass';

// Graphql
const menuFragment = graphql`
  fragment Menu on Taxonomy @relay(plural: true) {
    id
    index
    displayName
    url
  }
`;

// Flow
type Props = {
  currentCategoryIndex: number,
  preloadedQuery: preloadQueryType,
  setCurrentCategoryIndex: Function,
};

export default function Menu({
  preloadedQuery,
  currentCategoryIndex,
  setCurrentCategoryIndex,
}: Props) {
  const data = usePreloadedQuery(AppQuery, preloadedQuery);
  const taxonomyData = useFragment(menuFragment, data.taxonomy);
  const taxonomy = React.useMemo(() => {
    return [...taxonomyData].sort((a, b) => a.index - b.index);
  }, [taxonomyData]);

  return (
    <nav className={styles.menu}>
      {taxonomy.map((element) => (
        <MenuItem
          key={element.id}
          data={element}
          currentCategoryIndex={currentCategoryIndex}
          setCurrentCategoryIndex={setCurrentCategoryIndex}
        />
      ))}
    </nav>
  );
}
