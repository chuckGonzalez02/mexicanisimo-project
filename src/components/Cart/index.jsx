// @flow strict

// Librerias
import React from 'react';
import { useHistory } from 'react-router-dom';
import { animated, useTransition } from 'react-spring';
import { graphql, usePreloadedQuery, useFragment } from 'react-relay/hooks';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componentes
import Item from '../Item';
import Button from '../Button';

// Estilos
import styles from './Cart.atomic.sass';

// Artifacts
import AppQuery from '../../__generated__/App_Session_Query.graphql';

// Constantes
import { OPAQUE_STYLE, TRANSPARENT_STYLE } from '../../constants';

// Graphql
const fragment = graphql`
  fragment Cart on Cart {
    id
    qty
    items {
      ...Item
    }
    subtotal
    delivery
    sales
    total
  }
`;

// Flow
type Props = {
  preloadedQuery: preloadQueryType,
};

export default function Cart({ preloadedQuery }: Props) {
  const history = useHistory();
  const data = usePreloadedQuery(AppQuery, preloadedQuery);
  const cart = useFragment(fragment, data.viewer.cart);
  const { items, sales, subtotal, total } = cart;

  // eslint-disable-next-line no-underscore-dangle
  const transitions = useTransition(items, (item) => item.__id, {
    from: TRANSPARENT_STYLE,
    enter: OPAQUE_STYLE,
    leave: TRANSPARENT_STYLE,
  });

  function checkout() {
    history.push('/checkout');
  }

  return (
    <section>
      <div className={styles.head}>
        <h3 className={styles.title}>Mi pedido</h3>
        <div className={styles.count}>{`Total $${total}`}</div>
      </div>
      <div className={styles.itemsContainer}>
        {transitions.map(({ item, props, key }) => (
          // eslint-disable-next-line no-underscore-dangle
          <animated.div key={key} className={styles.item} style={props}>
            <Item data={item} />
          </animated.div>
        ))}
        {!!transitions.length && (
          <>
            <div>
              <div className={styles.account}>
                <span className={styles.accountLabel}>Subtotal</span>
                <span className={styles.accountValue}>{`$${subtotal}`}</span>
              </div>
              {!!sales && (
                <div className={styles.account}>
                  <span className={styles.accountLabel}>Descuentos</span>
                  <span className={styles.accountValueForSales}>
                    <strong>{`$${sales}`}</strong>
                  </span>
                </div>
              )}
              <div className={styles.account}>
                <span className={styles.accountLabel}>Total</span>
                <span className={styles.accountValue}>
                  <strong>{`$${total}`}</strong>
                </span>
              </div>
            </div>
            <div className={styles.toolbar}>
              <Button onClick={checkout} className={styles.button}>
                Terminar mi pedido
              </Button>
            </div>
          </>
        )}
      </div>

      {!transitions.length && (
        <div className={styles.empty}>
          <i className={`material-icons-outlined ${styles.icon}`}>shopping_basket</i>
          <div className={styles.emptyText}>
            <span>Aún no has agregado</span>
            <br />
            <span>nada a tu pedido</span>
          </div>
        </div>
      )}
    </section>
  );
}
