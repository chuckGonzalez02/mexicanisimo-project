// @flow

// Librerias
import React from 'react';
import { graphql, useFragment, useMutation } from 'react-relay/hooks';

// Flow
import type { Story$ref as storyDataRef } from '../../__generated__/Story.graphql';
import type { Item$ref as itemRef } from '../../__generated__/Item.graphql';

// Graphql
import itemFragment from '../../__generated__/Item.graphql';

// Componentes
import Button from '../Button';
import Waiting from '../Waiting';

// Estilos
import styles from './Story.atomic.sass';

// Flow
type Props = {
  play: Function,
  pause: Function,
  product: storyDataRef,
  itemData?: null | { fragmentRefs: itemRef },
};

// Graphql
const productFragment = graphql`
  fragment Story on Product {
    id
    sku
    uri
    name
    image
    priceFormatted
    listPriceFormatted
    hasDiscount
    discountRate
    description
  }
`;

const mutation = graphql`
  mutation Story_AddToCart_Mutation($sku: String, $qty: Int) {
    viewer {
      id
      cart(sku: $sku, qty: $qty) {
        id
        qty
        updatedAt
        items {
          sku
          qty
          price
          listPrice
          total
          subtotal
          sales
          data {
            name
            uri
            image
          }
        }
        sales
        subtotal
        total
      }
    }
  }
`;

export default function Story({ product, itemData, play, pause }: Props) {
  const data = useFragment(productFragment, product);
  const item = useFragment(itemFragment, itemData);
  const [commit, commitIsPending] = useMutation(mutation);

  const qty = (item && item.qty) || 0;
  const { id, name, image, description, discountRate, priceFormatted, listPriceFormatted } = data;

  function stopPropagation(e) {
    e.stopPropagation();
  }

  function addToCart() {
    commit({
      variables: {
        sku: id,
        qty: qty + 1,
      },
    });
  }

  return (
    <div className={styles.container}>
      <img alt={name} src={image} className={styles.image} />
      <div className={styles.vignette} />
      <div
        aria-hidden
        className={styles.data}
        onClick={stopPropagation}
        onMouseEnter={pause}
        onMouseLeave={play}
      >
        {!!discountRate && (
          <div className={styles.discountData}>
            <div className={styles.discountRate}>
              <i className={`material-icons-outlined ${styles.tagDiscount}`}>local_offer</i>
              <span>{`${discountRate}%`}</span>
            </div>
          </div>
        )}
        <h2 className={styles.title}>{name}</h2>
        <p className={styles.description}>{description}</p>
        {!!discountRate && <div className={styles.listPrice}>{listPriceFormatted}</div>}
        <div className={styles.price}>{priceFormatted}</div>
        <div className={styles.toolbar}>
          <Button onClick={addToCart}>
            {commitIsPending ? (
              <div className={styles.waitingIndicator}>
                <Waiting />
              </div>
            ) : (
              <i className={`${styles.cartIcon} material-icons-outlined`}>shopping_basket</i>
            )}
            <span>Agregar a mi pedido</span>
          </Button>
        </div>
      </div>
    </div>
  );
}

Story.defaultProps = {
  itemData: null,
};
