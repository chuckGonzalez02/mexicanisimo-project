// @flow

// Librerias
import React from 'react';

// Estilos
import styles from './MenuItem.atomic.sass';

// Componentes
import InteractiveElement from '../InteractiveElement';

type MenuItemType = {
  id: string,
  url: string,
  index: number,
  displayName: string,
};

type Props = {
  data: MenuItemType,
  currentCategoryIndex: number,
  setCurrentCategoryIndex: Function,
};

export default function MenuItem({ data, currentCategoryIndex, setCurrentCategoryIndex }: Props) {
  const { id, index, displayName } = data;
  const match = index === currentCategoryIndex;

  function setNewIndex() {
    setCurrentCategoryIndex(index);
  }

  return (
    <div className={styles.element}>
      <InteractiveElement onClick={setNewIndex} className={styles.link}>
        <div className={styles.thumb}>
          <img
            alt={displayName}
            className={styles.image}
            src={`https://redcube.io/mexicanisimo/assets/${id}.jpg`}
          />
        </div>
        <span className={styles.label}>{displayName}</span>
      </InteractiveElement>
      {match && <div className={styles.bullet} />}
    </div>
  );
}
