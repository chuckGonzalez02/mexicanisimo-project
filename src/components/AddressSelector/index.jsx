// @flow

// Librerias
import React from 'react';
import { useHistory } from 'react-router-dom';
import { usePreloadedQuery } from 'react-relay/hooks';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Estilos
import styles from './AddressSelector.atomic.sass';

// Constantes
import { SELECT_NON_VALUE } from '../../constants';

const TRANSITION_CONFIG = {
  timeoutMs: 8000,
};

// Flow
type Props = {|
  query: any,
  to?: string | null,
  onChange?: (string) => void,
  queryReference: preloadQueryType,
|};

function AddressSelector({ to, query, onChange, queryReference }: Props) {
  const [startTransition, transitionIsPending] = React.useTransition(TRANSITION_CONFIG);
  const history = useHistory();
  const data = usePreloadedQuery(query, queryReference);
  const address = data?.viewer?.address || data?.viewer?.checkout?.address;
  const addresses = data?.viewer?.addresses || data?.viewer?.checkout?.addresses || [];
  const selectedAddress = address?.id || SELECT_NON_VALUE;

  const [selectedAddressId, setSelectedAddressId] = React.useState(selectedAddress);
  const hasAddress = Boolean(address);

  function addAddress() {
    history.push(`/direcciones${to ? `?to=${to}` : ''}`);
  }

  function handleOnMouseDown(e) {
    e.preventDefault();
  }

  function handleAddressSelect(e) {
    const { value } = e.target;

    if (value === SELECT_NON_VALUE) addAddress();
    else {
      setSelectedAddressId(value);
      if (onChange) {
        startTransition(() => {
          onChange(value);
        });
      }
    }
  }

  return (
    <div className={`${styles.container} ${transitionIsPending ? styles.containerPending : ''}`}>
      <div className={styles.content}>
        {!hasAddress && (
          <button
            type="button"
            onClick={addAddress}
            className={styles.button}
            onMouseDown={handleOnMouseDown}
          >
            <i className={`${styles.plusIcon} material-icons-outlined`}>add</i>
            <div className={styles.label}>Agregar dirección</div>
          </button>
        )}

        {hasAddress && (
          <>
            <select
              className={styles.select}
              value={selectedAddressId}
              onChange={handleAddressSelect}
            >
              {addresses.map((addressOption) => (
                <option key={addressOption.id} value={addressOption.id}>
                  {`${addressOption.street} ${addressOption.externalNumber}`}
                </option>
              ))}
              <option value={SELECT_NON_VALUE}>Crear nueva dirección</option>
            </select>
            <i className={`${styles.selectIcon} material-icons-outlined`}>keyboard_arrow_down</i>
          </>
        )}
      </div>
    </div>
  );
}

AddressSelector.defaultProps = {
  to: null,
  onChange: undefined,
};

function AddressSelectorLoader() {
  return (
    <div className={styles.container}>
      <div className={styles.contentLoader} />
    </div>
  );
}

export default function AddressSelectorInterface({ to, query, onChange, queryReference }: Props) {
  if (!queryReference) return <AddressSelectorLoader />;

  return (
    <React.Suspense fallback={<AddressSelectorLoader />}>
      <AddressSelector to={to} query={query} onChange={onChange} queryReference={queryReference} />
    </React.Suspense>
  );
}

AddressSelectorInterface.defaultProps = {
  to: null,
  onChange: undefined,
};
