// @flow

// Librerias
import React from 'react';

// Componentes
import Lottie from '../Lottie';
import animationData from './data.json';

// Estilos
import styles from './loader.atomic.sass';
import animatedStyles from './loader.module.sass';

const defaultOptions = {
  animationData,
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

type Props = {
  width?: number,
  height?: number,
};

export default function Loader({ width = 38, height = 38 }: Props) {
  const remSize = parseInt(window.getComputedStyle(document.documentElement).fontSize, 10);

  return (
    <div className={`${styles.container} ${animatedStyles.container}`}>
      <Lottie
        options={defaultOptions}
        height={height * remSize}
        width={width * remSize}
        isStopped={false}
        isPaused={false}
      />
    </div>
  );
}

Loader.defaultProps = {
  width: 38,
  height: 38,
};
