// @flow

// Librerias
import * as React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { graphql, usePreloadedQuery, useFragment, useRefetchableFragment } from 'react-relay/hooks';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componentes
import Menu from '../Menu';
import Story from '../Story';
import InteractiveElement from '../InteractiveElement';

// Hooks
import useInterval from '../../hooks/useInterval';

// Artifacts
import AppContentQuery from '../../__generated__/App_Content_Query.graphql';
import SessionContentQuery from '../../__generated__/App_Session_Query.graphql';
import menuFragment from '../../__generated__/Menu.graphql';
import cartFragment from '../../__generated__/Cart.graphql';

// Estilos
import styles from './Stories.atomic.sass';
import animationStyles from './Stories.module.sass';

// Constantes
import { ENTER, ESCAPE } from '../../constants';

// Flow
type Props = {
  contentQueryReference: preloadQueryType,
  sessionQueryReference: preloadQueryType,
};

// Graphql fragments
const plpFragment = graphql`
  fragment Stories on Query @refetchable(queryName: "ProductListRefetchableFragmentQuery") {
    productList(defaultFilter: $defaultFilter) {
      edges {
        node {
          ...Story
        }
        cursor
      }
    }
  }
`;

// Misc
const TRANSITION_CONFIG = {
  timeoutMs: 3000,
};

// Segundos
const SLIDE_DURATION = 6;
const FPS = 125;
const RATE = 1000 / FPS;
const STEPS = FPS * SLIDE_DURATION;
const REAL_STEPS = STEPS + 1;
const STEP_SIZE = 1 / STEPS;

export default function Stories({ sessionQueryReference, contentQueryReference }: Props) {
  // $FlowFixMe
  const checkoutMatch = useRouteMatch('/checkout/:step?');
  const isInCheckout = !!checkoutMatch;

  const [startTransition, transitionIsPending] = React.useTransition(TRANSITION_CONFIG);
  const [forcePause, setForcePause] = React.useState(isInCheckout);
  const [allowRate, setAllowRate] = React.useState(!isInCheckout);
  const [step, setStep] = React.useState(1);
  const [currentStory, setCurrentStory] = React.useState(0);
  const data = usePreloadedQuery(AppContentQuery, contentQueryReference);
  const sessionData = usePreloadedQuery(SessionContentQuery, sessionQueryReference);
  const taxonomyData = useFragment(menuFragment, data.taxonomy);
  const cartData = useFragment(cartFragment, sessionData.viewer.cart);
  const taxonomy = React.useMemo(() => {
    return [...taxonomyData].sort((a, b) => a.index - b.index);
  }, [taxonomyData]);

  const [plp, refetchPLP] = useRefetchableFragment(plpFragment, data);
  const { edges } = plp.productList;
  const [currentCategoryIndex, setCurrentCategoryIndex] = React.useState(0);
  const currentCategory = taxonomy[currentCategoryIndex];
  const currentEdge = edges[currentStory] || {};
  const productData = currentEdge.node;
  const itemData = productData
    ? // eslint-disable-next-line no-underscore-dangle
      (cartData.items || []).find((item) => item.__id === `CartItem:${productData.__id}`)
    : null;

  React.useEffect(() => {
    setForcePause(isInCheckout);
    setAllowRate(!isInCheckout);
  }, [isInCheckout]);

  const edgesLength = edges.length;
  const tabs = React.useMemo(() => {
    const els = [];

    for (let i = 0; i < edgesLength; i += 1) {
      els.push(i);
    }

    return els;
  }, [edgesLength]);

  function navigate(delta) {
    setStep(1);
    const newIndex = currentStory + delta;

    if (newIndex > -1) {
      const newItem = edges[newIndex];
      if (newItem) return setCurrentStory(newIndex);
    }

    const taxonomyLength = taxonomy.length;
    const newCategoryIndex =
      (taxonomy.indexOf(currentCategory) + delta + taxonomyLength) % taxonomyLength;

    setAllowRate(false);
    return startTransition(() => {
      refetchPLP(
        {
          defaultFilter: {
            name: 'scategory.keyword',
            value: taxonomy[newCategoryIndex].id,
          },
        },
        {
          fetchPolicy: 'store-or-network',
        },
      );

      setCurrentCategoryIndex(newCategoryIndex);
      setAllowRate(true);
      setCurrentStory(0);
      setStep(1);
    });
  }

  function updateCategory(index: number): void {
    setStep(1);

    setAllowRate(false);
    startTransition(() => {
      refetchPLP(
        {
          defaultFilter: {
            name: 'scategory.keyword',
            value: taxonomy[index].id,
          },
        },
        {
          fetchPolicy: 'store-or-network',
        },
      );

      setCurrentCategoryIndex(index);
      setAllowRate(true);
      setCurrentStory(0);
      setStep(1);
    });
  }

  useInterval(
    () => {
      if (!step) navigate(1);
      setStep((count) => (count + 1) % REAL_STEPS);
    },
    allowRate ? RATE : null,
  );

  function goBack() {
    navigate(-1);
  }

  function goForward() {
    navigate(1);
  }

  function toggleTimer() {
    const isPause = forcePause;
    setForcePause(!isPause);
    setAllowRate(isPause);
  }

  function play() {
    if (forcePause) {
      toggleTimer();
    }
  }

  function pause() {
    if (!forcePause) {
      toggleTimer();
    }
  }

  function preventOutline(e) {
    e.preventDefault();
  }

  function handleKeyPress(e) {
    const { key } = e;

    if (key === ENTER) toggleTimer();
    else if (key === ESCAPE) e.target.blur();
  }

  return (
    <>
      <div className={`${styles.container} ${transitionIsPending ? styles.containerInactive : ''}`}>
        {!!transitionIsPending && (
          <div className={`${styles.progressContainer} ${animationStyles.opacity}`}>
            <div className={`${styles.progress} ${animationStyles.progress}`} />
          </div>
        )}
        {currentCategory && (
          <div className={styles.categoryData}>
            <div className={styles.categoryElement}>
              <img
                className={styles.categoryImage}
                alt={currentCategory.displayName}
                src={`https://redcube.io/mexicanisimo/assets/${currentCategory.id}.jpg`}
              />
              <span className={styles.categoryDisplayname}>{currentCategory.displayName}</span>
            </div>
            {forcePause && (
              <div
                tabIndex="0"
                role="button"
                onClick={toggleTimer}
                onKeyPress={handleKeyPress}
                onMouseDown={preventOutline}
                className={styles.timerState}
              >
                Pausado
              </div>
            )}
          </div>
        )}
        <div className={styles.storiesContainer}>
          <div className={styles.arrowContainer}>
            <InteractiveElement className={styles.cartIconContainer} onClick={goBack}>
              <i className={`material-icons-outlined ${styles.arrow}`}>arrow_back</i>
            </InteractiveElement>
          </div>
          <div
            className={styles.tabContainer}
            style={{ gridTemplateColumns: `repeat(${edgesLength}, 1fr)` }}
          >
            {tabs.map((tab) => {
              const style =
                tab === currentStory ? { transform: `scaleX(${step * STEP_SIZE})` } : {};

              return (
                <div key={tab} className={styles.tab}>
                  <div
                    style={style}
                    className={`${styles.timer} ${(tab > currentStory && styles.inactiveTimer) ||
                      (tab < currentStory && styles.activeTimer) ||
                      styles.currentTimer}`}
                  />
                </div>
              );
            })}
          </div>
          {!!productData && (
            <div
              tabIndex="0"
              role="button"
              onClick={toggleTimer}
              onKeyPress={handleKeyPress}
              onMouseDown={preventOutline}
              className={styles.storyMultimediaContainer}
            >
              <Story product={productData} itemData={itemData} play={play} pause={pause} />
            </div>
          )}
          <div className={styles.arrowContainer}>
            <InteractiveElement className={styles.cartIconContainer} onClick={goForward}>
              <i className={`material-icons-outlined ${styles.arrow}`}>arrow_forward</i>
            </InteractiveElement>
          </div>
        </div>
      </div>
      <div className={styles.menu}>
        <Menu
          preloadedQuery={contentQueryReference}
          setCurrentCategoryIndex={updateCategory}
          currentCategoryIndex={currentCategoryIndex}
        />
      </div>
    </>
  );
}
