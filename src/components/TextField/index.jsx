// @flow

// Librerias
import React from 'react';

// Estilos
import styles from './index.module.sass';

import type { inputRef } from '../../hooks/useInput';

type Props = {
  label?: string,
  className?: string,
  input: inputRef,
};

export default function TextField({ input, className, label }: Props) {
  const inputData = input || {};
  const propsInput = inputData.input;

  return (
    <div
      className={`${styles.TextFieldRoot} ${(!!propsInput.error && styles.error) ||
        ''} ${(propsInput.disabled && styles.disabled) || ''} ${className || ''}`}
    >
      {!!label && <label htmlFor={inputData.id || ''}>{label}</label>}
      <input
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...propsInput}
        id={inputData.id}
        value={inputData.displayValue}
      />
      {!!propsInput.error && <div className={styles.errorDescription}>{propsInput.error}</div>}
    </div>
  );
}

TextField.defaultProps = {
  className: '',
  label: '',
};
