// @flow

// Librerias
import * as React from 'react';
import { useSpring, animated } from 'react-spring';
import { graphql, useFragment, useMutation } from 'react-relay/hooks';

// Componentes
import InteractiveElement from '../InteractiveElement';

// Estilos
import styles from './Item.atomic.sass';

// Graphql
import addToCartMutation from '../../__generated__/Story_AddToCart_Mutation.graphql';

// Flow
import type { Item$ref as itemRef } from '../../__generated__/Item.graphql';

// Graphql
const fragment = graphql`
  fragment Item on CartItem {
    id
    sku
    qty
    price
    listPrice
    total
    subtotal
    sales
    data {
      name
      image
    }
  }
`;

// Flow
type Props = {
  data: itemRef,
};

export default function Item({ data }: Props) {
  const item = useFragment(fragment, data);
  const { sku, qty, total, price } = item;
  const { name, image } = item.data;
  const [commit, isFlight] = useMutation(addToCartMutation);
  const { x } = useSpring({ x: qty % 2 });

  function add(): void {
    commit({
      variables: {
        sku,
        qty: qty + 1,
      },
    });
  }

  function remove(): void {
    commit({
      variables: {
        sku,
        qty: qty - 1,
      },
    });
  }

  return (
    <div className={styles.row}>
      <div className={styles.thumbContainer}>
        <animated.div
          className={styles.quantity}
          style={{
            transform: x
              .interpolate({
                range: [0, 0.5, 1],
                output: [0, 8, 0],
              })
              .interpolate((scale) => `translateY(-${scale}px)`),
          }}
        >
          {qty}
        </animated.div>
        <img className={styles.thumb} src={image.replace('?size=1000', '?size=100')} alt={name} />
      </div>
      <div className={styles.data}>
        <div className={styles.name}>{name}</div>
        <div className={styles.pricing}>
          <div>Precio por pieza</div>
          <div>{`$${price}`}</div>
        </div>
        <div className={styles.total}>
          <div>Subtotal :</div>
          <div>
            <strong>{`$${total}`}</strong>
          </div>
        </div>
        <div className={`${styles.toolbar} ${isFlight ? styles.inactiveToolbar : ''}`}>
          <InteractiveElement className={styles.arrowElement} onClick={remove}>
            <i className={`material-icons-outlined ${styles.cartIcon}`}>remove</i>
          </InteractiveElement>
          <span className={styles.qty}>{`${qty} pieza${qty > 1 ? 's' : ''}`}</span>
          <InteractiveElement className={styles.arrowElement} onClick={add}>
            <i className={`material-icons-outlined ${styles.cartIcon}`}>add</i>
          </InteractiveElement>
        </div>
      </div>
    </div>
  );
}
