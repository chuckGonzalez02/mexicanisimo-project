// @flow

// Librerias
import React from 'react';
import { Redirect, Link, useRouteMatch } from 'react-router-dom';
import { graphql, useMutation, usePreloadedQuery } from 'react-relay/hooks';

// Flow
import type { loadQuery as loadQueryType } from 'react-relay/hooks';

// Componentes
import Modal from '../../components/Modal';
import TextField from '../../components/TextField';
import AddressSelector from '../../components/AddressSelector';
import InteractiveElement from '../../components/InteractiveElement';

// Hooks
import useInput from '../../hooks/useInput';
import validateData from '../../hooks/validateData';

// Estilos
import styles from './Checkout.atomic.sass';

// Artifacts
import CheckoutQuery from '../../__generated__/App_Checkout_Query.graphql';

// Misc
import loadScript from '../../misc/loadScript';
import setCurrency from '../../misc/setCurrency';

// Constantes
import { GMAPS_KEY, NETWORK_ONLY, STORAGE_COUPON } from '../../constants';

// Graphql
const paypalMutation = graphql`
  mutation Checkout_Paypal_Mutation(
    $address: String
    $userData: userDataInput
    $couponRequested: String
    $mode: String
  ) {
    viewer {
      id
      paypalTransaction(
        address: $address
        userData: $userData
        couponRequested: $couponRequested
        mode: $mode
      )
    }
  }
`;

const paypalTransactionMutation = graphql`
  mutation Checkout_PaypalTransaction_Mutation(
    $address: String
    $userData: userDataInput
    $billingData: billingDataInput
    $couponRequested: String
    $paymentProvider: String
    $orderID: String
    $mode: String
  ) {
    viewer {
      id
      checkout(
        address: $address
        userData: $userData
        billingData: $billingData
        couponRequested: $couponRequested
      ) {
        id
        transaction(paymentProvider: $paymentProvider, orderID: $orderID, mode: $mode) {
          id
          status
          success
          orderID
          orderError
          errorMessage
          status_detail
        }
      }
    }
  }
`;

// Constantes
const TRANSITION_CONFIG = {
  timeoutMs: 8000,
};

const isLive = process.env.NODE_ENV === 'production';
const modeString = isLive ? 'live' : 'sandbox';

// Flow
type Props = {|
  loadQuery: Function,
  preloadedQuery: loadQueryType,
|};

// Momentos de entrega
const NOW = 'NOW';
const PLANNING = 'PLANNING';

export default function Checkout({ loadQuery, preloadedQuery }: Props) {
  const [startTransition] = React.useTransition(TRANSITION_CONFIG);
  const data = usePreloadedQuery(CheckoutQuery, preloadedQuery);
  const viewer = data?.viewer;
  const name = viewer?.name;
  const checkout = viewer?.checkout;
  const address = checkout?.address;
  const cart = checkout?.cart;
  const deliveryMetadata = checkout?.deliveryMetadata;
  const geopoint = address?.geopoint;
  const { lat, lng } = geopoint || {};
  const { durationInMinutes } = deliveryMetadata || {};
  const promoCode = sessionStorage.getItem(STORAGE_COUPON);
  const [hasPromoCode, setHasPromoCode] = React.useState(promoCode);
  const paymentMatch = !!useRouteMatch('/checkout/:payment');
  const [deliveryMoment, setDeliveryMoment] = React.useState(NOW);
  const deliveryIsNow = deliveryMoment === NOW;

  const [commitPaypal] = useMutation(paypalMutation);
  const [commitPaypalTransaction] = useMutation(paypalTransactionMutation);

  const promo = useInput({
    value: sessionStorage.getItem(STORAGE_COUPON) || '',
    name: 'promo',
    toLowerCase: true,
    placeholder: 'ej. PROMO1',
  });

  const renderPaypalButtons = React.useCallback(() => {
    window.paypal
      .Buttons({
        createOrder: () =>
          new Promise((resolve, reject) => {
            commitPaypal({
              variables: {
                mode: modeString,
                address: address.id,
                couponRequested: promoCode,
                userData: {
                  email: viewer?.email || '',
                  phone: viewer?.phone || '',
                  displayName: `${viewer.name || ''} ${viewer.lastName || ''}`,
                },
              },
              onCompleted(response, errors) {
                if (response && response.viewer && response.viewer.paypalTransaction)
                  resolve(response.viewer.paypalTransaction);
                else reject(errors || null);
              },
              onError(err) {
                // eslint-disable-next-line no-console
                console.trace({ err });
                reject(err);
              },
            });
          }),
        onApprove: ({ orderID }) => {
          commitPaypalTransaction({
            variables: {
              orderID,
              mode: modeString,
              address: address.id,
              userData: {
                email: viewer?.email || '',
                displayName: `${viewer.name || ''} ${viewer.lastName || ''}`,
                phone: viewer?.phone || '',
              },
              billingData: {
                rfc: null,
                billingName: null,
                billingAddressLine1: null,
                billingAddressLine2: null,
              },
              couponRequested: promoCode,
              paymentProvider: 'PAYPAL',
            },
            onCompleted: (response, errors) => {
              if (errors) {
                // eslint-disable-next-line no-use-before-define
                renderPaypalButtons();
              } else {
                const transaction =
                  (response &&
                    response.viewer &&
                    response.viewer.checkout &&
                    response.viewer.checkout.transaction) ||
                  null;

                const confirmedOrderId = (transaction && transaction.orderID) || null;

                if (confirmedOrderId) window.location.href = `/confirmacion/${confirmedOrderId}`;
                else {
                  // eslint-disable-next-line no-console
                  console.log('Loading false');
                }
              }
            },
            onError: (err) => {
              // eslint-disable-next-line no-console
              console.trace({ err });
            },
          });
        },
      })
      .render('#paypal-button-container');
  }, [address, viewer, commitPaypal, commitPaypalTransaction, promoCode]);

  React.useEffect(() => {
    if (paymentMatch) {
      if (!window.paypal) {
        loadScript(
          `https://www.paypal.com/sdk/js?currency=MXN&locale=es_MX&client-id=${
            isLive
              ? 'AdzOp9bpeYSFnoW5II5NnWvZcF2kl13A7KKpyBH8KeZsG0JcGNkoNk_dRPVI9j62KFxUEqqSs7olIiEc'
              : 'AaBWEv4JnvqZxNYL1vXNJDWZ8KV8t0XPWFgTkomqEbcspAB8cwDIMCvUbWudRWsBJF_0jehLPrKABUMT'
          }`,
          renderPaypalButtons,
        );
      } else {
        renderPaypalButtons();
      }
    }
  }, [paymentMatch, renderPaypalButtons]);

  if (!name) return <Redirect to="/registro?to=/checkout" />;
  if (!address) return <Redirect to="/direcciones?to=/checkout" />;

  function handleAddressChange(id: string): void {
    const coupon = sessionStorage.getItem(STORAGE_COUPON);
    loadQuery(
      { address: id, couponRequested: coupon || null },
      {
        fetchPolicy: NETWORK_ONLY,
      },
    );
  }

  function handlePromoSubmit(e) {
    e.preventDefault();

    const { data: promoData } = validateData([promo]);

    const code = promoData.promo || null;

    if (code) sessionStorage.setItem(STORAGE_COUPON, promoData.promo);
    else sessionStorage.removeItem(STORAGE_COUPON);

    setHasPromoCode(!!code);

    startTransition(() => {
      loadQuery(
        { couponRequested: code },
        {
          fetchPolicy: NETWORK_ONLY,
        },
      );
    });
  }

  function cleanPromo() {
    promo.setData((state) => ({ ...state, value: '', displayValue: '' }));

    sessionStorage.removeItem(STORAGE_COUPON);
    setHasPromoCode(false);

    startTransition(() => {
      loadQuery(
        { couponRequested: '' },
        {
          fetchPolicy: NETWORK_ONLY,
        },
      );
    });
  }

  function setDeliveryMomentToNow() {
    setDeliveryMoment(NOW);
  }

  function setDeliveryMomentToPlanning() {
    setDeliveryMoment(PLANNING);
  }

  if (paymentMatch)
    return (
      <Modal>
        <div className={styles.content}>
          <h1 className={styles.title}>Terminar pedido</h1>
          <div id="paypal-button-container" />
        </div>
      </Modal>
    );

  return (
    <Modal>
      <div className={styles.content}>
        <h1 className={styles.title}>Resumen de pedido</h1>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Momento de la entrega</h2>
          <div className={styles.options}>
            <InteractiveElement onClick={setDeliveryMomentToNow} avoidAnimation>
              <div
                className={`${styles.deliveryTimeOption} ${
                  deliveryIsNow
                    ? styles.deliveryTimeOptionActive
                    : styles.deliveryTimeOptionInactive
                }`}
              >
                <i
                  className={`material-icons-outlined ${styles.deliveryOptionIcon} ${
                    deliveryIsNow ? styles.deliveryOptionIconActive : ''
                  }`}
                >
                  query_builder
                </i>
                <span>Ahora mismo</span>
              </div>
            </InteractiveElement>
            <InteractiveElement onClick={setDeliveryMomentToPlanning} avoidAnimation>
              <div
                className={`${styles.deliveryTimeOption} ${
                  !deliveryIsNow
                    ? styles.deliveryTimeOptionActive
                    : styles.deliveryTimeOptionInactive
                }`}
              >
                <i
                  className={`material-icons-outlined ${styles.deliveryOptionIcon} ${
                    !deliveryIsNow ? styles.deliveryOptionIconActive : ''
                  }`}
                >
                  today
                </i>
                <span>Programar</span>
              </div>
            </InteractiveElement>
          </div>
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>
            {deliveryIsNow ? 'Tiempo estimado de entrega' : 'Fecha de entrega'}
          </h2>
          <div className={styles.timig}>
            {`${durationInMinutes} - ${durationInMinutes + 10} minutos`}
          </div>
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Dirección de entrega</h2>
          <AddressSelector
            to="/checkout"
            query={CheckoutQuery}
            queryReference={preloadedQuery}
            onChange={handleAddressChange}
          />
          {!!geopoint && (
            <img
              alt=""
              className={styles.mapImage}
              src={`https://maps.googleapis.com/maps/api/staticmap?size=384x200&maptype=roadmap&markers=${lat},${lng}&scale=2&key=${GMAPS_KEY}`}
            />
          )}
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Detalle de los platillos</h2>
          <div>
            {cart.items.map((item) => (
              <div key={item.id} className={styles.item}>
                <div className={styles.itemData}>
                  <span className={styles.qty}>{item.qty}</span>
                  <span className={styles.name}>{item.data.name}</span>
                </div>
                <div className={styles.price}>{`$${setCurrency(item.total)}`}</div>
              </div>
            ))}
          </div>
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Código promocional</h2>
          {!hasPromoCode && (
            <form className={styles.promoForm} onSubmit={handlePromoSubmit}>
              <TextField input={promo.input} />
              {!!promo?.input?.displayValue && (
                <button type="submit" className={styles.buttonPromo}>
                  Canjear código
                </button>
              )}
            </form>
          )}
          {hasPromoCode && (
            <div className={styles.promoDisplay}>
              <div className={styles.promoData}>
                <i className={`${styles.tagPromo} material-icons-outlined`}>local_offer</i>
                <div className={styles.promoLabel}>{promoCode}</div>
              </div>
              <button type="button" className={styles.deletePromo} onClick={cleanPromo}>
                <i className={`${styles.deleteIcon} material-icons-outlined`}>delete</i>
              </button>
            </div>
          )}
        </div>
        <div className={`${styles.section} ${styles.borderTop}`}>
          <div className={styles.accountDetail}>
            <div className={styles.accountingProp}>Subtotal</div>
            <div className={styles.accountingValue}>{`$${setCurrency(cart.subtotal)}`}</div>
          </div>
          <div className={styles.accountDetail}>
            <div className={styles.accountingProp}>Envío</div>
            <div className={styles.accountingValue}>{`$${setCurrency(cart.delivery)}`}</div>
          </div>
          <div className={styles.accountDetail}>
            <div className={styles.accountingProp}>Descuentos</div>
            <div className={styles.accountingValue}>{`$${setCurrency(cart.sales)}`}</div>
          </div>
          <div className={styles.accountDetail}>
            <div className={styles.accountingProp}>
              <strong className={styles.accountingPropBold}>Total</strong>
            </div>
            <div className={styles.accountingValue}>{`$${setCurrency(cart.total)}`}</div>
          </div>
        </div>
      </div>
      <Link to="/checkout/payment" className={styles.checkoutLink}>
        <div className={styles.checkout}>
          <div className={styles.cta}>
            <span>Terminar la compra</span>
            <i className={`${styles.iconCta} material-icons-outlined`}>arrow_forward</i>
          </div>
          <div className={styles.ctaPrice}>{`$${setCurrency(cart.total)}`}</div>
        </div>
      </Link>
    </Modal>
  );
}
