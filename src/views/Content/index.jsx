// @flow

// Librerias
import React from 'react';

// Flow
import type { loadQuery as preloadQueryType } from 'react-relay/hooks';

// Componentes
import Header from '../../components/Header';
import Showroom from '../../components/Showroom';

// Estilos
import styles from './Content.atomic.sass';

// Flow
type Props = {|
  addressesQuery: any,
  loadAddressQuery: Function,
  contentQueryReference: preloadQueryType,
  sessionQueryReference: preloadQueryType,
  addressQueryReference: preloadQueryType,
|};

export default function Content({
  addressesQuery,
  loadAddressQuery,
  sessionQueryReference,
  contentQueryReference,
  addressQueryReference,
}: Props) {
  return (
    <section className={styles.view}>
      <Header
        addressesQuery={addressesQuery}
        loadAddressQuery={loadAddressQuery}
        sessionQueryReference={sessionQueryReference}
        addressQueryReference={addressQueryReference}
      />
      <Showroom
        sessionQueryReference={sessionQueryReference}
        contentQueryReference={contentQueryReference}
      />
    </section>
  );
}
