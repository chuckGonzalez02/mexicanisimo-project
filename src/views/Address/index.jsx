// @flow

// Librerias
import React from 'react';
import { graphql, useQueryLoader, usePreloadedQuery, useMutation } from 'react-relay/hooks';

// Estilos
import styles from './Address.atomic.sass';

// Componentes
import Modal from '../../components/Modal';
import Button from '../../components/Button';
import TextField from '../../components/TextField';

// Hooks
import useInput from '../../hooks/useInput';
import validateData from '../../hooks/validateData';
import type { useInputRef } from '../../hooks/useInput';

// Artifacts
import type { Address_Places_Query as addressPlacesQueryType } from '../../__generated__/Address_Places_Query.graphql';
import type { Address_Geocode_Query as addressGeocodeQueryType } from '../../__generated__/Address_Geocode_Query.graphql';

// Constantes
import { ESCAPE, ENTER, ARROW_DOWN, ARROW_UP, GMAPS_KEY } from '../../constants';

// Graphql
const placesQuery = graphql`
  query Address_Places_Query($query: String) {
    places(query: $query) {
      id
      title
      description
    }
  }
`;

const getAddressFromPlaceQuery = graphql`
  query Address_Geocode_Query($place: String) {
    getAddressFromPlace(place: $place) {
      externalNumber
      neighborhood
      street
      postalCode
      place
      geopoint {
        lat
        lng
      }
      neighborhoodData {
        id
        city
        displayname
        state
        municipality
      }
    }
  }
`;

const saveAddressMutation = graphql`
  mutation Address_Upsert_Mutation($address: AddressInput) {
    viewer {
      id
      address(address: $address) {
        id
        street
        neighborhood
        postalCode
        externalNumber
        internalNumber
        reference
        selected
      }
    }
  }
`;

type Props = {|
  queryReference: any,
  onClick: (place: string) => void,
  disposeQuery: () => void,
|};

function Autocomplete({ queryReference, onClick, disposeQuery }: Props) {
  const [selected, setSelected] = React.useState(-1);
  const data = usePreloadedQuery<addressPlacesQueryType>(placesQuery, queryReference);
  const { places } = data;
  const placesLength = places.length;

  return (
    <div className={styles.autocomplete}>
      {places.map((place, index) => {
        function selectPlace(): void {
          const selectedPlace = places[selected];
          onClick(selectedPlace?.id || place.id);
        }

        function handleKeyDown(e) {
          const { key } = e;

          if (key === ESCAPE) disposeQuery();
          else if (key === ENTER) selectPlace();

          const isArrowDown = key === ARROW_DOWN;

          if (isArrowDown || key === ARROW_UP) {
            const direction = isArrowDown ? 1 : -1;
            const newIndex = (selected + placesLength + direction) % placesLength;

            try {
              const els = document.querySelectorAll('.placesAutoCompleteOption');
              const el = els[newIndex];

              el.focus();
            } finally {
              setSelected(newIndex);
            }
          }
        }

        function handleFocus() {
          setSelected(index);
        }

        function handleMouseEnter(e) {
          setSelected(index);
          e.target.focus();
        }

        return (
          <div
            tabIndex="0"
            role="menuitem"
            key={place.id}
            onFocus={handleFocus}
            onClick={selectPlace}
            className={`placesAutoCompleteOption ${styles.option} ${
              index === selected ? styles.optionActive : ''
            }`}
            onKeyDown={handleKeyDown}
            onMouseEnter={handleMouseEnter}
          >
            <i className={`material-icons-outlined ${styles.optionIcon}`}>location_on</i>
            <div className={styles.optionData}>
              <div className={styles.optionDataTitle}>{place.title}</div>
              <div>{place.description}</div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const TRANSITION_CONFIG = {
  timeoutMs: 3000,
};

type AddressDescriptionProps = {|
  setAddress: any,
  queryReference: any,
|};

function AddressDescription({ queryReference, setAddress }: AddressDescriptionProps) {
  const data = usePreloadedQuery<addressGeocodeQueryType>(getAddressFromPlaceQuery, queryReference);
  const address = data?.getAddressFromPlace;
  const geopoint = address?.geopoint;
  const { lat, lng } = geopoint || {};

  React.useEffect(() => {
    if (data) setAddress(data);
  }, [setAddress, data]);

  const displayname = `${address.street} ${address.externalNumber}, ${address.neighborhood}, ${address.neighborhoodData.municipality}`;
  return (
    <div>
      <div className={styles.addressDisplay}>{displayname}</div>
      <img
        alt={displayname}
        className={styles.mapImage}
        src={`https://maps.googleapis.com/maps/api/staticmap?size=384x200&maptype=roadmap&markers=${lat},${lng}&scale=2&key=${GMAPS_KEY}`}
      />
    </div>
  );
}

function AddressPlaceholder() {
  return (
    <div>
      <div className={styles.placeholderContainer}>
        <div className={styles.placeholderElement} />
        <div className={styles.placeholderElement} />
      </div>
      <div className={styles.mapImage} />
    </div>
  );
}

type AddressFormProps = {|
  setAddress: any,
  queryReference: any,
  reference: useInputRef,
  handleSubmit: (any) => void,
  internalNumber: useInputRef,
  restartView: Function,
|};

function AddressForm({
  reference,
  setAddress,
  restartView,
  handleSubmit,
  queryReference,
  internalNumber,
}: AddressFormProps) {
  return (
    <>
      <div className={styles.addressDisplayTitle}>Dirección</div>
      <React.Suspense fallback={<AddressPlaceholder />}>
        <AddressDescription queryReference={queryReference} setAddress={setAddress} />
      </React.Suspense>
      <div className={styles.input}>
        <TextField input={internalNumber.input} label="Depto/Oficina/Piso" />
      </div>
      <TextField input={reference.input} label="Nombre del negocio o edificio/Referencia" />
      <div className={styles.toolbar}>
        <Button onClick={handleSubmit} className={styles.button}>
          Continuar
        </Button>
        <Button inline onClick={restartView} className={styles.button}>
          Regresar
        </Button>
      </div>
    </>
  );
}

export default function Address() {
  const to = new URLSearchParams(window.location.search).get('to');
  const [address, setAddress] = React.useState(null);
  const [startTransition] = React.useTransition(TRANSITION_CONFIG);

  const query = useInput({
    value: '',
    name: 'query',
    placeholder: 'ej. Av Insurgentes 5000',
    errors: {
      requiredError: 'Es necesario que busques tu dirección',
    },
  });

  const internalNumber = useInput({
    name: 'internalNumber',
    placeholder: 'ej. Interior 5',
    value: '',
  });

  const reference = useInput({
    name: 'reference',
    placeholder: 'ej. Portón rojo',
    required: false,
    value: '',
  });

  const queryValue = query.input.displayValue;
  const [queryReference, loadQuery, disposeQuery] = useQueryLoader(placesQuery);
  const [queryGeocodeReference, loadGeocodeQuery, disposeGeocodeQuery] = useQueryLoader(
    getAddressFromPlaceQuery,
  );
  const [commitAddress] = useMutation(saveAddressMutation);

  React.useEffect(() => {
    if (queryValue.length > 3)
      startTransition(() => {
        loadQuery({ query: queryValue });
      });
    else disposeQuery();
  }, [startTransition, loadQuery, queryValue, disposeQuery]);

  React.useEffect(() => {
    if (queryGeocodeReference) disposeQuery();
  }, [queryGeocodeReference, disposeQuery]);

  function handleSubmit(e) {
    e.preventDefault();

    const { data, errors } = validateData([internalNumber, reference]);
    const addressFromPlace = address ? address.getAddressFromPlace : {};

    if (!errors) {
      commitAddress({
        variables: {
          address: {
            externalNumber: addressFromPlace.externalNumber,
            internalNumber: data.internalNumber,
            neighborhood: addressFromPlace.neighborhood,
            postalCode: addressFromPlace.postalCode,
            reference: data.reference,
            street: addressFromPlace.street,
            place: addressFromPlace.place,
            geopoint: addressFromPlace.geopoint,
            selected: true,
          },
        },
        onCompleted(response) {
          if (response?.viewer?.address) {
            window.location.href = to || '/';
          }
          // eslint-disable-next-line no-alert
          else alert(999);
        },
      });
    }
  }

  function handlePlaceSelect(id: string): void {
    startTransition(() => {
      loadGeocodeQuery({ place: id });
    });
  }

  function restartView() {
    disposeGeocodeQuery();
    query.setData({ value: '', displayValue: '', error: null });
  }

  return (
    <Modal>
      <div className={styles.content}>
        <h1 className={styles.title}>Ingresa una nueva dirección</h1>
        <form className={styles.form} onSubmit={handleSubmit} autoComplete="off">
          {!queryGeocodeReference && (
            <div className={styles.input}>
              <TextField input={query.input} label="Ingresa la dirección de entrega" />
              {!!queryReference && (
                <React.Suspense fallback={<div />}>
                  <Autocomplete
                    queryReference={queryReference}
                    onClick={handlePlaceSelect}
                    disposeQuery={disposeQuery}
                  />
                </React.Suspense>
              )}
            </div>
          )}
          {!!queryGeocodeReference && (
            <AddressForm
              reference={reference}
              setAddress={setAddress}
              handleSubmit={handleSubmit}
              internalNumber={internalNumber}
              queryReference={queryGeocodeReference}
              restartView={restartView}
            />
          )}
        </form>
      </div>
    </Modal>
  );
}
