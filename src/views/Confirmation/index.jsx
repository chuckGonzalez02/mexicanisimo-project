// @flow

// Librerias
import React from 'react';
import { usePreloadedQuery } from 'react-relay/hooks';

// Flow
import type { loadQuery as loadQueryType } from 'react-relay/hooks';

// Componentes
import Modal from '../../components/Modal';

// Estilos
import styles from './Confirmation.atomic.sass';

// Misc
import setCurrency from '../../misc/setCurrency';

// Flow
type Props = {|
  query: any,
  queryReference: loadQueryType,
|};

export default function Confirmation({ query, queryReference }: Props) {
  const data = usePreloadedQuery(query, queryReference);
  const { order = {} } = data?.viewer || {};
  const { address, cart } = order;
  const { street, externalNumber, internalNumber, neighborhood, postalCode } = address;

  return (
    <Modal>
      <div className={styles.content}>
        <h1 className={styles.title}>Confirmación de pedido</h1>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Pedido completado</h2>
          <div className={styles.value}>Entrega estimada</div>
          <div className={styles.value}>{`Pedido no. ${order.id}`}</div>
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Dirección de entrega</h2>
          <div className={styles.value}>
            {`${street} ${externalNumber} ${internalNumber}, ${neighborhood}, CP ${postalCode}`}
          </div>
        </div>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Detalle de los platillos</h2>
          <div>
            {cart.items.map((item) => (
              <div key={item.id} className={styles.item}>
                <span className={styles.qty}>{item.qty}</span>
                <span className={styles.name}>{item.data.name}</span>
              </div>
            ))}
          </div>
        </div>
        <div className={`${styles.section} ${styles.borderTop}`}>
          <div className={styles.accountDetail}>
            <div className={styles.accountingProp}>
              <strong className={styles.accountingPropBold}>Total</strong>
            </div>
            <div className={styles.accountingValue}>{`$${setCurrency(cart.total)}`}</div>
          </div>
        </div>
      </div>
    </Modal>
  );
}
