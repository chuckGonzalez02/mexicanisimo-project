// @flow

// Librerias
import React from 'react';
import { useRouteMatch, Link } from 'react-router-dom';
import { graphql, useMutation } from 'react-relay/hooks';

// Estilos
import styles from './Sign.atomic.sass';

// Componentes
import Modal from '../../components/Modal';
import Button from '../../components/Button';
import TextField from '../../components/TextField';

// Hooks
import useInput from '../../hooks/useInput';
import validateData from '../../hooks/validateData';

// Graphql
const sendVerificationCodeMutation = graphql`
  mutation Sign_SendVerificationCode_Mutation($phone: String) {
    sendVerificationCode(phone: $phone) {
      isNewUser
    }
  }
`;

const validateVerificationCodeMutation = graphql`
  mutation Sign_ValidateVerificationCode_Mutation($phone: String, $verificationCode: String) {
    validateVerificationCode(phone: $phone, verificationCode: $verificationCode) {
      id
      token
    }
  }
`;

const createAccountMutation = graphql`
  mutation Sign_CreateAccount_Mutation(
    $accessToken: String
    $name: String
    $lastName: String
    $email: String
    $password: String
    $phone: String
  ) {
    register(
      token: $accessToken
      name: $name
      lastName: $lastName
      email: $email
      password: $password
      phone: $phone
    ) {
      id
      token
    }
  }
`;

const loginMutation = graphql`
  mutation Sign_Login_Mutation($email: String, $password: String) {
    login(email: $email, password: $password) {
      id
      token
    }
  }
`;

export default function Sign() {
  const [step, setStep] = React.useState(0);
  const [accessToken, setAccessToken] = React.useState(null);
  const loginMatch = useRouteMatch('/iniciar-sesion');
  const isLogin = !!loginMatch;
  const [isNewUser, setIsNewUser] = React.useState(!isLogin);

  const [sendVerificationCodeCommit] = useMutation(sendVerificationCodeMutation);
  const [validateVerificationCodeCommit] = useMutation(validateVerificationCodeMutation);
  const [createAccountCommit] = useMutation(createAccountMutation);
  const [loginCommit] = useMutation(loginMutation);

  const to = new URLSearchParams(window.location.search).get('to');

  const phone = useInput({
    name: 'phone',
    placeholder: 'ej. 55 1234 5678',
    // eslint-disable-next-line no-useless-escape
    regexp: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
    validateEvent: 'blur',
    errors: {
      requiredError: 'Es necesario que ingreses un teléfono celular',
      defaultError: 'Ingresa un teléfono celular válido',
    },
    required: true,
  });

  const email = useInput({
    name: 'email',
    placeholder: 'ej. juan@gmail.com',
    // eslint-disable-next-line no-useless-escape
    regexp: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    validateEvent: 'blur',
    errors: {
      requiredError: 'Es necesario que ingreses un correo',
      defaultError: 'Ingresa un correo válido',
    },
    required: true,
  });

  const verificationCode = useInput({
    maxlength: 4,
    minlength: 4,
    regexp: /\d/,
    regexpOverwrite: /\d*/,
    placeholder: 'ej. 1234',
    validateEvent: 'change',
    name: 'verificationCode',
    errors: {
      requiredError: 'Es necesario que ingreses tu código',
    },
    required: true,
  });

  const password = useInput({
    name: 'password',
    minlength: 8,
    placeholder: 'ej. ********',
    type: 'password',
    validateEvent: 'blur',
    errors: {
      requiredError: 'Es necesario que crees una contraseña',
      minlengthError: 'Ingresa por lo menos 8 caractéres',
    },
    required: true,
  });

  const validatePassword = useInput({
    name: 'validatePassword',
    minlength: 8,
    placeholder: 'ej. ********',
    type: 'password',
    validateEvent: 'blur',
    errors: {
      defaultError: 'Verifica que hayas escrito correctamente tu contraseña',
      requiredError: 'Es necesario que confirmes tu contraseña',
      minlengthError: 'Ingresa por lo menos 8 caractéres',
    },
    required: !isLogin,
    customValidation(value) {
      return value === password.input.displayValue || (isLogin && !value);
    },
  });

  const name = useInput({
    name: 'name',
    placeholder: 'ej. José Arcadio',
    errors: {
      requiredError: 'Es necesario que ingreses tu nombre',
    },
    required: !isLogin,
  });

  const lastName = useInput({
    name: 'lastName',
    placeholder: 'ej. Buendía',
    errors: {
      requiredError: 'Es necesario que ingreses tu apellido',
    },
    required: !isLogin,
  });

  function handleSubmit(e) {
    e.preventDefault();

    if (step === 0) {
      const { data, errors } = validateData([phone]);

      if (!errors) {
        sendVerificationCodeCommit({
          variables: { phone: data.phone },
          onCompleted(response) {
            setStep(1);
            setIsNewUser(response?.sendVerificationCode?.isNewUser);
          },
        });
      }
    } else if (step === 1) {
      const { data, errors } = validateData([phone, verificationCode]);

      if (!errors)
        validateVerificationCodeCommit({
          variables: { phone: data.phone, verificationCode: data.verificationCode },
          onCompleted(response) {
            if (response?.validateVerificationCode?.token) {
              if (isNewUser) {
                setAccessToken(response.validateVerificationCode.token);
                setStep(2);
              } else {
                localStorage.setItem('token', response.validateVerificationCode.token);
                window.location.href = to || '/';
              }
            }
            // eslint-disable-next-line no-alert
            else alert(999);
          },
        });
    } else if (step === 2) {
      const { data, errors } = validateData([email, password, validatePassword]);

      if (!errors) {
        if (isLogin) {
          loginCommit({
            variables: {
              email: email.input.displayValue,
              password: data.password,
            },
            onCompleted(response) {
              if (response?.login?.token) {
                localStorage.setItem('token', response.login.token);
                window.location.href = to || '/';
              }
              // eslint-disable-next-line no-alert
              else alert(999);
            },
          });
        } else setStep(3);
      }
    } else if (step === 3) {
      const { data, errors } = validateData([name, lastName]);

      if (!errors) {
        createAccountCommit({
          variables: {
            name: data.name,
            lastName: data.lastName,
            email: email.input.displayValue,
            password: password.input.displayValue || null,
            accessToken: accessToken || localStorage.getItem('token') || null,
            phone: phone.input.displayValue,
          },
          onCompleted(response) {
            if (response?.register?.token) {
              localStorage.setItem('token', response.register.token);
              window.location.href = to || '/';
            }
            // eslint-disable-next-line no-alert
            else alert(999);
          },
        });
      }
    }
  }

  return (
    <Modal>
      <div className={styles.content}>
        <h1 className={styles.title}>{isLogin ? 'Inicia sesión' : 'Regístrate'}</h1>
        <form className={styles.form} onSubmit={handleSubmit}>
          {step === 0 && (
            <div className={styles.input}>
              <TextField input={phone.input} label="Número de télefono" />
            </div>
          )}
          {step === 1 && (
            <>
              <div className={styles.input}>
                <TextField input={verificationCode.input} label="Código de verificación" />
              </div>
            </>
          )}
          {step === 2 && (
            <>
              <div className={styles.input}>
                <TextField input={email.input} label="Correo electrónico" />
              </div>
              <div className={styles.input}>
                <TextField input={password.input} label="Contraseña (Mínimo 8 caracteres)" />
              </div>
              {!isLogin && (
                <div className={styles.input}>
                  <TextField input={validatePassword.input} label="Confirma tu contraseña" />
                </div>
              )}
            </>
          )}
          {!isLogin && step === 3 && (
            <>
              <div className={styles.input}>
                <TextField input={name.input} label="Nombre completo" />
              </div>
              <div className={styles.input}>
                <TextField input={lastName.input} label="Apellido" />
              </div>
            </>
          )}
          <div className={styles.toolbar}>
            <Button onClick={handleSubmit} className={styles.button}>
              Continuar
            </Button>
          </div>
          <div className={styles.switch}>
            <span className={styles.switchLabel}>
              {isLogin ? '¿Aún no tienes una cuenta?' : '¿Ya tienes una cuenta?'}
            </span>
            <Link
              to={`${isLogin ? '/registro' : '/iniciar-sesion'}${to ? `?to=${to}` : ''}`}
              className={styles.switchLink}
            >
              {isLogin ? 'Crea una cuenta' : 'Inicia sesión'}
            </Link>
          </div>
        </form>
      </div>
    </Modal>
  );
}
