// @flow

// Librerias
import React from 'react';

// Estilos
import styles from './Spotify.atomic.sass';

// Constantes
import { SPOTIFY_PLAYLIST } from '../../constants';

// Iconos
import { ReactComponent as Code } from './sp-code.svg';
import { ReactComponent as LogoIcon } from '../../components/Icons/logo.svg';

export default function Spotify() {
  React.useEffect(() => {
    window.location.href = SPOTIFY_PLAYLIST;
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.viewport}>
        <div className={styles.content}>
          <LogoIcon className={styles.logo} />
          <h1 className={styles.title}>¡Buen provecho!</h1>
          <p className={styles.text}>
            Preparamos nuestros alimentos con el cariño que te mereces, para que los compartas con
            la gente que amas.
          </p>
          <p className={styles.text}>
            Queremos que disfrutes tu comida, acompaña este momento con un poco de sazón musical:
          </p>
          <div className={styles.codeContainer}>
            <a href={SPOTIFY_PLAYLIST}>
              <Code className={styles.code} />
              <div className={styles.codeHelp}>O haz click para abrir Spotify</div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
