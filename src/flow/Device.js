// @flow

export type Device = {
  browser: string,
  device: string,
  height: number,
  isIE: boolean,
  isMobileAgent: boolean,
  isOrientationCapable: boolean,
  isTouch: boolean,
  orientation: string,
  os: string,
  screenSize: string,
  version: string,
  width: number,
  breakpoint: String,
};
