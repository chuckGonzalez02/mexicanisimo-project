export default function setCurrency(a) {
  const re = '\\d(?=(\\d{3})+\\.)';
  // eslint-disable-next-line no-bitwise
  return a.toFixed(Math.max(0, ~~2)).replace(new RegExp(re, 'g'), '$&,');
}
