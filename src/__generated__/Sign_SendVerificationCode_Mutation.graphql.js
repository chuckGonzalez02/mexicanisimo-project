/**
 * @flow
 * @relayHash e52d2464f1c75ae2ba6dc0b49ce8ae5a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Sign_SendVerificationCode_MutationVariables = {|
  phone?: ?string
|};
export type Sign_SendVerificationCode_MutationResponse = {|
  +sendVerificationCode: ?{|
    +isNewUser: ?boolean
  |}
|};
export type Sign_SendVerificationCode_Mutation = {|
  variables: Sign_SendVerificationCode_MutationVariables,
  response: Sign_SendVerificationCode_MutationResponse,
|};
*/


/*
mutation Sign_SendVerificationCode_Mutation(
  $phone: String
) {
  sendVerificationCode(phone: $phone) {
    isNewUser
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "phone",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "phone",
        "variableName": "phone"
      }
    ],
    "concreteType": "VerificationCode",
    "kind": "LinkedField",
    "name": "sendVerificationCode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "isNewUser",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Sign_SendVerificationCode_Mutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Sign_SendVerificationCode_Mutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "e52d2464f1c75ae2ba6dc0b49ce8ae5a",
    "metadata": {},
    "name": "Sign_SendVerificationCode_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd68fed9606cf277a4f03a8b48ba8cf97';

module.exports = node;
