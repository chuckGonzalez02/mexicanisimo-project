/**
 * @flow
 * @relayHash aeb00099f88d369e5246dce2d623be3c
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Sign_CreateAccount_MutationVariables = {|
  accessToken?: ?string,
  name?: ?string,
  lastName?: ?string,
  email?: ?string,
  password?: ?string,
  phone?: ?string,
|};
export type Sign_CreateAccount_MutationResponse = {|
  +register: ?{|
    +id: ?string,
    +token: ?string,
  |}
|};
export type Sign_CreateAccount_Mutation = {|
  variables: Sign_CreateAccount_MutationVariables,
  response: Sign_CreateAccount_MutationResponse,
|};
*/


/*
mutation Sign_CreateAccount_Mutation(
  $accessToken: String
  $name: String
  $lastName: String
  $email: String
  $password: String
  $phone: String
) {
  register(token: $accessToken, name: $name, lastName: $lastName, email: $email, password: $password, phone: $phone) {
    id
    token
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "accessToken",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "lastName",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "email",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "password",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "phone",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "email",
        "variableName": "email"
      },
      {
        "kind": "Variable",
        "name": "lastName",
        "variableName": "lastName"
      },
      {
        "kind": "Variable",
        "name": "name",
        "variableName": "name"
      },
      {
        "kind": "Variable",
        "name": "password",
        "variableName": "password"
      },
      {
        "kind": "Variable",
        "name": "phone",
        "variableName": "phone"
      },
      {
        "kind": "Variable",
        "name": "token",
        "variableName": "accessToken"
      }
    ],
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "register",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "token",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Sign_CreateAccount_Mutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Sign_CreateAccount_Mutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "aeb00099f88d369e5246dce2d623be3c",
    "metadata": {},
    "name": "Sign_CreateAccount_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'fd5cecccac801137c13904ddca455a77';

module.exports = node;
