/**
 * @flow
 * @relayHash 8a93ff7f226fb57f6eab4a29abda3c1c
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Sign_Login_MutationVariables = {|
  email?: ?string,
  password?: ?string,
|};
export type Sign_Login_MutationResponse = {|
  +login: ?{|
    +id: ?string,
    +token: ?string,
  |}
|};
export type Sign_Login_Mutation = {|
  variables: Sign_Login_MutationVariables,
  response: Sign_Login_MutationResponse,
|};
*/


/*
mutation Sign_Login_Mutation(
  $email: String
  $password: String
) {
  login(email: $email, password: $password) {
    id
    token
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "email",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "password",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "email",
        "variableName": "email"
      },
      {
        "kind": "Variable",
        "name": "password",
        "variableName": "password"
      }
    ],
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "login",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "token",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Sign_Login_Mutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Sign_Login_Mutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "8a93ff7f226fb57f6eab4a29abda3c1c",
    "metadata": {},
    "name": "Sign_Login_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '84622ed8d82a19866979b0102e8b3629';

module.exports = node;
