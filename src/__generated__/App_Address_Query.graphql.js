/**
 * @flow
 * @relayHash e68dadc52c0c37debe089bac9e928015
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AddressInput = {|
  externalNumber?: ?string,
  id?: ?string,
  internalNumber?: ?string,
  neighborhood?: ?string,
  reference?: ?string,
  street?: ?string,
  postalCode?: ?string,
  neighborhoodData?: ?NeighborhoodInput,
  selected?: ?boolean,
  place?: ?string,
  geopoint?: ?GeopointInput,
|};
export type NeighborhoodInput = {|
  id?: ?string,
  city?: ?string,
  displayname?: ?string,
  state?: ?string,
  municipality?: ?string,
|};
export type GeopointInput = {|
  lat?: ?number,
  lng?: ?number,
|};
export type App_Address_QueryVariables = {|
  address?: ?AddressInput
|};
export type App_Address_QueryResponse = {|
  +viewer: ?{|
    +id: ?string,
    +address: ?{|
      +id: ?string,
      +externalNumber: ?string,
      +internalNumber: ?string,
      +neighborhood: ?string,
      +postalCode: ?string,
      +reference: ?string,
      +street: ?string,
      +place: ?string,
      +geopoint: ?{|
        +lat: ?number,
        +lng: ?number,
      |},
    |},
    +addresses: ?$ReadOnlyArray<?{|
      +id: ?string,
      +externalNumber: ?string,
      +internalNumber: ?string,
      +neighborhood: ?string,
      +postalCode: ?string,
      +reference: ?string,
      +street: ?string,
      +place: ?string,
      +geopoint: ?{|
        +lat: ?number,
        +lng: ?number,
      |},
    |}>,
  |}
|};
export type App_Address_Query = {|
  variables: App_Address_QueryVariables,
  response: App_Address_QueryResponse,
|};
*/


/*
query App_Address_Query(
  $address: AddressInput
) {
  viewer {
    id
    address(address: $address) {
      id
      externalNumber
      internalNumber
      neighborhood
      postalCode
      reference
      street
      place
      geopoint {
        lat
        lng
      }
    }
    addresses {
      id
      externalNumber
      internalNumber
      neighborhood
      postalCode
      reference
      street
      place
      geopoint {
        lat
        lng
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "address",
    "type": "AddressInput"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = [
  (v1/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "externalNumber",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "internalNumber",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "neighborhood",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "postalCode",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "reference",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "street",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "place",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "Geopoint",
    "kind": "LinkedField",
    "name": "geopoint",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "lat",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "lng",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
],
v3 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "address",
            "variableName": "address"
          }
        ],
        "concreteType": "Address",
        "kind": "LinkedField",
        "name": "address",
        "plural": false,
        "selections": (v2/*: any*/),
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Address",
        "kind": "LinkedField",
        "name": "addresses",
        "plural": true,
        "selections": (v2/*: any*/),
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "App_Address_Query",
    "selections": (v3/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "App_Address_Query",
    "selections": (v3/*: any*/)
  },
  "params": {
    "id": "e68dadc52c0c37debe089bac9e928015",
    "metadata": {},
    "name": "App_Address_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '9ce32238762fa34388b3fd604394b0bd';

module.exports = node;
