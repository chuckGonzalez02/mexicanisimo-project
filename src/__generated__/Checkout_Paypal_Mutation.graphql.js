/**
 * @flow
 * @relayHash 3905a754d621f06d281f3eb3b838e12c
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type userDataInput = {|
  displayName?: ?string,
  email?: ?string,
  phone?: ?string,
|};
export type Checkout_Paypal_MutationVariables = {|
  address?: ?string,
  userData?: ?userDataInput,
  couponRequested?: ?string,
  mode?: ?string,
|};
export type Checkout_Paypal_MutationResponse = {|
  +viewer: ?{|
    +id: ?string,
    +paypalTransaction: ?string,
  |}
|};
export type Checkout_Paypal_Mutation = {|
  variables: Checkout_Paypal_MutationVariables,
  response: Checkout_Paypal_MutationResponse,
|};
*/


/*
mutation Checkout_Paypal_Mutation(
  $address: String
  $userData: userDataInput
  $couponRequested: String
  $mode: String
) {
  viewer {
    id
    paypalTransaction(address: $address, userData: $userData, couponRequested: $couponRequested, mode: $mode)
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "address",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userData",
    "type": "userDataInput"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "couponRequested",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "mode",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "address",
            "variableName": "address"
          },
          {
            "kind": "Variable",
            "name": "couponRequested",
            "variableName": "couponRequested"
          },
          {
            "kind": "Variable",
            "name": "mode",
            "variableName": "mode"
          },
          {
            "kind": "Variable",
            "name": "userData",
            "variableName": "userData"
          }
        ],
        "kind": "ScalarField",
        "name": "paypalTransaction",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Checkout_Paypal_Mutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Checkout_Paypal_Mutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "3905a754d621f06d281f3eb3b838e12c",
    "metadata": {},
    "name": "Checkout_Paypal_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c28cb1af006e2d13f2f912c853621fd4';

module.exports = node;
