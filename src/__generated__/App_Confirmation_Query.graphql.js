/**
 * @flow
 * @relayHash bb8611ecb0fec492e50fae3506df8c4e
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type App_Confirmation_QueryVariables = {|
  order?: ?string
|};
export type App_Confirmation_QueryResponse = {|
  +viewer: ?{|
    +id: ?string,
    +order: ?{|
      +id: ?string,
      +isEmpty: ?boolean,
      +status: ?string,
      +chargeDescription: ?string,
      +cart: ?{|
        +id: ?string,
        +items: ?$ReadOnlyArray<?{|
          +id: ?string,
          +qty: ?number,
          +total: ?any,
          +data: ?{|
            +name: ?string
          |},
        |}>,
        +subtotal: ?any,
        +delivery: ?any,
        +sales: ?any,
        +total: ?any,
      |},
      +address: ?{|
        +id: ?string,
        +externalNumber: ?string,
        +internalNumber: ?string,
        +neighborhood: ?string,
        +postalCode: ?string,
        +reference: ?string,
        +street: ?string,
        +geopoint: ?{|
          +lat: ?number,
          +lng: ?number,
        |},
      |},
      +transaction: ?{|
        +id: ?string,
        +status: ?string,
        +success: ?boolean,
        +orderID: ?string,
        +status_detail: ?string,
        +date_created: ?any,
      |},
    |},
  |}
|};
export type App_Confirmation_Query = {|
  variables: App_Confirmation_QueryVariables,
  response: App_Confirmation_QueryResponse,
|};
*/


/*
query App_Confirmation_Query(
  $order: String
) {
  viewer {
    id
    order(order: $order) {
      id
      isEmpty
      status
      chargeDescription
      cart {
        id
        items {
          id
          qty
          total
          data {
            name
          }
        }
        subtotal
        delivery
        sales
        total
      }
      address {
        id
        externalNumber
        internalNumber
        neighborhood
        postalCode
        reference
        street
        geopoint {
          lat
          lng
        }
      }
      transaction {
        id
        status
        success
        orderID
        status_detail
        date_created
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "order",
    "type": "String"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "total",
  "storageKey": null
},
v4 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "order",
            "variableName": "order"
          }
        ],
        "concreteType": "Checkout",
        "kind": "LinkedField",
        "name": "order",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isEmpty",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "chargeDescription",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Cart",
            "kind": "LinkedField",
            "name": "cart",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "CartItem",
                "kind": "LinkedField",
                "name": "items",
                "plural": true,
                "selections": [
                  (v1/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "qty",
                    "storageKey": null
                  },
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Product",
                    "kind": "LinkedField",
                    "name": "data",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "name",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "subtotal",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "delivery",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "sales",
                "storageKey": null
              },
              (v3/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Address",
            "kind": "LinkedField",
            "name": "address",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "externalNumber",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "internalNumber",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "neighborhood",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "postalCode",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "reference",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "street",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Geopoint",
                "kind": "LinkedField",
                "name": "geopoint",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "lat",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "lng",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Transaction",
            "kind": "LinkedField",
            "name": "transaction",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "success",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "orderID",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "status_detail",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "date_created",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "App_Confirmation_Query",
    "selections": (v4/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "App_Confirmation_Query",
    "selections": (v4/*: any*/)
  },
  "params": {
    "id": "bb8611ecb0fec492e50fae3506df8c4e",
    "metadata": {},
    "name": "App_Confirmation_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '7a5f81d02b8d5848bba8353f3f75f19b';

module.exports = node;
