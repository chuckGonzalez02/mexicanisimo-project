/**
 * @flow
 * @relayHash 68c5631336098b87dde0ddaab31f1e9a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type userDataInput = {|
  displayName?: ?string,
  email?: ?string,
  phone?: ?string,
|};
export type billingDataInput = {|
  billingAddressLine1?: ?string,
  billingAddressLine2?: ?string,
  billingName?: ?string,
  rfc?: ?string,
|};
export type Checkout_PaypalTransaction_MutationVariables = {|
  address?: ?string,
  userData?: ?userDataInput,
  billingData?: ?billingDataInput,
  couponRequested?: ?string,
  paymentProvider?: ?string,
  orderID?: ?string,
  mode?: ?string,
|};
export type Checkout_PaypalTransaction_MutationResponse = {|
  +viewer: ?{|
    +id: ?string,
    +checkout: ?{|
      +id: ?string,
      +transaction: ?{|
        +id: ?string,
        +status: ?string,
        +success: ?boolean,
        +orderID: ?string,
        +orderError: ?string,
        +errorMessage: ?string,
        +status_detail: ?string,
      |},
    |},
  |}
|};
export type Checkout_PaypalTransaction_Mutation = {|
  variables: Checkout_PaypalTransaction_MutationVariables,
  response: Checkout_PaypalTransaction_MutationResponse,
|};
*/


/*
mutation Checkout_PaypalTransaction_Mutation(
  $address: String
  $userData: userDataInput
  $billingData: billingDataInput
  $couponRequested: String
  $paymentProvider: String
  $orderID: String
  $mode: String
) {
  viewer {
    id
    checkout(address: $address, userData: $userData, billingData: $billingData, couponRequested: $couponRequested) {
      id
      transaction(paymentProvider: $paymentProvider, orderID: $orderID, mode: $mode) {
        id
        status
        success
        orderID
        orderError
        errorMessage
        status_detail
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "address",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userData",
    "type": "userDataInput"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "billingData",
    "type": "billingDataInput"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "couponRequested",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "paymentProvider",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "orderID",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "mode",
    "type": "String"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "address",
            "variableName": "address"
          },
          {
            "kind": "Variable",
            "name": "billingData",
            "variableName": "billingData"
          },
          {
            "kind": "Variable",
            "name": "couponRequested",
            "variableName": "couponRequested"
          },
          {
            "kind": "Variable",
            "name": "userData",
            "variableName": "userData"
          }
        ],
        "concreteType": "Checkout",
        "kind": "LinkedField",
        "name": "checkout",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "mode",
                "variableName": "mode"
              },
              {
                "kind": "Variable",
                "name": "orderID",
                "variableName": "orderID"
              },
              {
                "kind": "Variable",
                "name": "paymentProvider",
                "variableName": "paymentProvider"
              }
            ],
            "concreteType": "Transaction",
            "kind": "LinkedField",
            "name": "transaction",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "status",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "success",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "orderID",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "orderError",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "errorMessage",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "status_detail",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Checkout_PaypalTransaction_Mutation",
    "selections": (v2/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Checkout_PaypalTransaction_Mutation",
    "selections": (v2/*: any*/)
  },
  "params": {
    "id": "68c5631336098b87dde0ddaab31f1e9a",
    "metadata": {},
    "name": "Checkout_PaypalTransaction_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '1da6f66e389cf6f0a5d79a2be752914f';

module.exports = node;
