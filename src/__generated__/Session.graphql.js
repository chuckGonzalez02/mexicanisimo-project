/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type Session$ref: FragmentReference;
declare export opaque type Session$fragmentType: Session$ref;
export type Session = {|
  +id: ?string,
  +name: ?string,
  +lastName: ?string,
  +$refType: Session$ref,
|};
export type Session$data = Session;
export type Session$key = {
  +$data?: Session$data,
  +$fragmentRefs: Session$ref,
  ...
};
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "Session",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lastName",
      "storageKey": null
    }
  ],
  "type": "Viewer"
};
// prettier-ignore
(node/*: any*/).hash = '96aeeca5ecacef154f038ab1fd1a5408';

module.exports = node;
