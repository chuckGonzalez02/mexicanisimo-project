/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { Item$ref } from "./Item.graphql";
import type { FragmentReference } from "relay-runtime";
declare export opaque type Cart$ref: FragmentReference;
declare export opaque type Cart$fragmentType: Cart$ref;
export type Cart = {|
  +id: ?string,
  +qty: ?number,
  +items: ?$ReadOnlyArray<?{|
    +$fragmentRefs: Item$ref
  |}>,
  +subtotal: ?any,
  +delivery: ?any,
  +sales: ?any,
  +total: ?any,
  +$refType: Cart$ref,
|};
export type Cart$data = Cart;
export type Cart$key = {
  +$data?: Cart$data,
  +$fragmentRefs: Cart$ref,
  ...
};
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "Cart",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "qty",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "CartItem",
      "kind": "LinkedField",
      "name": "items",
      "plural": true,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "Item"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "subtotal",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "delivery",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "sales",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "total",
      "storageKey": null
    }
  ],
  "type": "Cart"
};
// prettier-ignore
(node/*: any*/).hash = '672c9f01d6435f9eeccfd40b96584ccc';

module.exports = node;
