/**
 * @flow
 * @relayHash be0b456c35803be0d72a5fb759493bde
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type Stories$ref: FragmentReference;
declare export opaque type Stories$fragmentType: Stories$ref;
export type Filter = {|
  name?: ?string,
  value?: ?string,
|};
export type ProductListRefetchableFragmentQueryVariables = {|
  defaultFilter?: ?Filter
|};
export type ProductListRefetchableFragmentQueryResponse = {|
  +$fragmentRefs: Stories$ref
|};
export type ProductListRefetchableFragmentQuery = {|
  variables: ProductListRefetchableFragmentQueryVariables,
  response: ProductListRefetchableFragmentQueryResponse,
|};
*/


/*
query ProductListRefetchableFragmentQuery(
  $defaultFilter: Filter
) {
  ...Stories
}

fragment Stories on Query {
  productList(defaultFilter: $defaultFilter) {
    edges {
      node {
        ...Story
      }
      cursor
    }
  }
}

fragment Story on Product {
  id
  sku
  uri
  name
  image
  priceFormatted
  listPriceFormatted
  hasDiscount
  discountRate
  description
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "defaultFilter",
    "type": "Filter"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ProductListRefetchableFragmentQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "Stories"
      }
    ],
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ProductListRefetchableFragmentQuery",
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "defaultFilter",
            "variableName": "defaultFilter"
          }
        ],
        "concreteType": "productListConnection",
        "kind": "LinkedField",
        "name": "productList",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "productListEdge",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Product",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "id",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "sku",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "uri",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "name",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "image",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "priceFormatted",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "listPriceFormatted",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hasDiscount",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "discountRate",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "description",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "cursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "id": "be0b456c35803be0d72a5fb759493bde",
    "metadata": {
      "derivedFrom": "Stories",
      "isRefetchableQuery": true
    },
    "name": "ProductListRefetchableFragmentQuery",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '9a7c68dadd082ddba592272c487d1c58';

module.exports = node;
