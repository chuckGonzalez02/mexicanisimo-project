/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { Story$ref } from "./Story.graphql";
import type { FragmentReference } from "relay-runtime";
import type { Stories$ref, Stories$fragmentType } from "./ProductListRefetchableFragmentQuery.graphql";
export type { Stories$ref, Stories$fragmentType };
export type Stories = {|
  +productList: ?{|
    +edges: ?$ReadOnlyArray<?{|
      +node: ?{|
        +$fragmentRefs: Story$ref
      |},
      +cursor: ?any,
    |}>
  |},
  +$refType: Stories$ref,
|};
export type Stories$data = Stories;
export type Stories$key = {
  +$data?: Stories$data,
  +$fragmentRefs: Stories$ref,
  ...
};
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "defaultFilter",
      "type": "Filter"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "refetch": {
      "connection": null,
      "fragmentPathInResult": [],
      "operation": require('./ProductListRefetchableFragmentQuery.graphql.js')
    }
  },
  "name": "Stories",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Variable",
          "name": "defaultFilter",
          "variableName": "defaultFilter"
        }
      ],
      "concreteType": "productListConnection",
      "kind": "LinkedField",
      "name": "productList",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "productListEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Product",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "Story"
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query"
};
// prettier-ignore
(node/*: any*/).hash = '9a7c68dadd082ddba592272c487d1c58';

module.exports = node;
