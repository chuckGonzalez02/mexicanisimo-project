/**
 * @flow
 * @relayHash 8743a93a309e000c3545187e103f370d
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Address_Geocode_QueryVariables = {|
  place?: ?string
|};
export type Address_Geocode_QueryResponse = {|
  +getAddressFromPlace: ?{|
    +externalNumber: ?string,
    +neighborhood: ?string,
    +street: ?string,
    +postalCode: ?string,
    +place: ?string,
    +geopoint: ?{|
      +lat: ?number,
      +lng: ?number,
    |},
    +neighborhoodData: ?{|
      +id: ?string,
      +city: ?string,
      +displayname: ?string,
      +state: ?string,
      +municipality: ?string,
    |},
  |}
|};
export type Address_Geocode_Query = {|
  variables: Address_Geocode_QueryVariables,
  response: Address_Geocode_QueryResponse,
|};
*/


/*
query Address_Geocode_Query(
  $place: String
) {
  getAddressFromPlace(place: $place) {
    externalNumber
    neighborhood
    street
    postalCode
    place
    geopoint {
      lat
      lng
    }
    neighborhoodData {
      id
      city
      displayname
      state
      municipality
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "place",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "place",
        "variableName": "place"
      }
    ],
    "concreteType": "Address",
    "kind": "LinkedField",
    "name": "getAddressFromPlace",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "externalNumber",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "neighborhood",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "street",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "postalCode",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "place",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Geopoint",
        "kind": "LinkedField",
        "name": "geopoint",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "lat",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "lng",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Neighborhood",
        "kind": "LinkedField",
        "name": "neighborhoodData",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "city",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "displayname",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "state",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "municipality",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Address_Geocode_Query",
    "selections": (v1/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Address_Geocode_Query",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "8743a93a309e000c3545187e103f370d",
    "metadata": {},
    "name": "Address_Geocode_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '62dbdef145d8cde5cc47a845e1102586';

module.exports = node;
