/**
 * @flow
 * @relayHash 75c95d755242a1fe76f073eb15b79d76
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Address_Places_QueryVariables = {|
  query?: ?string
|};
export type Address_Places_QueryResponse = {|
  +places: ?$ReadOnlyArray<?{|
    +id: ?string,
    +title: ?string,
    +description: ?string,
  |}>
|};
export type Address_Places_Query = {|
  variables: Address_Places_QueryVariables,
  response: Address_Places_QueryResponse,
|};
*/


/*
query Address_Places_Query(
  $query: String
) {
  places(query: $query) {
    id
    title
    description
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "query",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "query",
        "variableName": "query"
      }
    ],
    "concreteType": "Place",
    "kind": "LinkedField",
    "name": "places",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "title",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Address_Places_Query",
    "selections": (v1/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Address_Places_Query",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "75c95d755242a1fe76f073eb15b79d76",
    "metadata": {},
    "name": "Address_Places_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c880f63799b7414a155389797fc8c3ac';

module.exports = node;
