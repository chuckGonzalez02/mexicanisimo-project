/**
 * @flow
 * @relayHash d9494dee1c35490394b3632da9e9c538
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type App_Checkout_QueryVariables = {|
  address?: ?string,
  couponRequested?: ?string,
|};
export type App_Checkout_QueryResponse = {|
  +viewer: ?{|
    +id: ?string,
    +name: ?string,
    +lastName: ?string,
    +phone: ?string,
    +email: ?string,
    +checkout: ?{|
      +id: ?string,
      +isEmpty: ?boolean,
      +cart: ?{|
        +id: ?string,
        +items: ?$ReadOnlyArray<?{|
          +id: ?string,
          +qty: ?number,
          +total: ?any,
          +data: ?{|
            +name: ?string
          |},
        |}>,
        +subtotal: ?any,
        +delivery: ?any,
        +sales: ?any,
        +total: ?any,
      |},
      +address: ?{|
        +id: ?string,
        +externalNumber: ?string,
        +internalNumber: ?string,
        +neighborhood: ?string,
        +postalCode: ?string,
        +reference: ?string,
        +street: ?string,
        +selected: ?boolean,
        +place: ?string,
        +geopoint: ?{|
          +lat: ?number,
          +lng: ?number,
        |},
      |},
      +addresses: ?$ReadOnlyArray<?{|
        +id: ?string,
        +externalNumber: ?string,
        +internalNumber: ?string,
        +neighborhood: ?string,
        +postalCode: ?string,
        +reference: ?string,
        +street: ?string,
        +selected: ?boolean,
        +place: ?string,
        +geopoint: ?{|
          +lat: ?number,
          +lng: ?number,
        |},
      |}>,
      +deliveryMetadata: ?{|
        +durationInMinutes: ?number,
        +standardFee: ?number,
        +extraKilometersFee: ?number,
      |},
    |},
  |}
|};
export type App_Checkout_Query = {|
  variables: App_Checkout_QueryVariables,
  response: App_Checkout_QueryResponse,
|};
*/


/*
query App_Checkout_Query(
  $address: String
  $couponRequested: String
) {
  viewer {
    id
    name
    lastName
    phone
    email
    checkout(address: $address, couponRequested: $couponRequested) {
      id
      isEmpty
      cart {
        id
        items {
          id
          qty
          total
          data {
            name
          }
        }
        subtotal
        delivery
        sales
        total
      }
      address {
        id
        externalNumber
        internalNumber
        neighborhood
        postalCode
        reference
        street
        selected
        place
        geopoint {
          lat
          lng
        }
      }
      addresses {
        id
        externalNumber
        internalNumber
        neighborhood
        postalCode
        reference
        street
        selected
        place
        geopoint {
          lat
          lng
        }
      }
      deliveryMetadata {
        durationInMinutes
        standardFee
        extraKilometersFee
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "address",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "couponRequested",
    "type": "String"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "total",
  "storageKey": null
},
v4 = [
  (v1/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "externalNumber",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "internalNumber",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "neighborhood",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "postalCode",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "reference",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "street",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "selected",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "place",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "Geopoint",
    "kind": "LinkedField",
    "name": "geopoint",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "lat",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "lng",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
],
v5 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      (v2/*: any*/),
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "lastName",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "phone",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "email",
        "storageKey": null
      },
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "address",
            "variableName": "address"
          },
          {
            "kind": "Variable",
            "name": "couponRequested",
            "variableName": "couponRequested"
          }
        ],
        "concreteType": "Checkout",
        "kind": "LinkedField",
        "name": "checkout",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isEmpty",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Cart",
            "kind": "LinkedField",
            "name": "cart",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "CartItem",
                "kind": "LinkedField",
                "name": "items",
                "plural": true,
                "selections": [
                  (v1/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "qty",
                    "storageKey": null
                  },
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Product",
                    "kind": "LinkedField",
                    "name": "data",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "subtotal",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "delivery",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "sales",
                "storageKey": null
              },
              (v3/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Address",
            "kind": "LinkedField",
            "name": "address",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Address",
            "kind": "LinkedField",
            "name": "addresses",
            "plural": true,
            "selections": (v4/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "DeliveryMetadata",
            "kind": "LinkedField",
            "name": "deliveryMetadata",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "durationInMinutes",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "standardFee",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "extraKilometersFee",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "App_Checkout_Query",
    "selections": (v5/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "App_Checkout_Query",
    "selections": (v5/*: any*/)
  },
  "params": {
    "id": "d9494dee1c35490394b3632da9e9c538",
    "metadata": {},
    "name": "App_Checkout_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '79721710fd86812f5ea75e4f6451f2c0';

module.exports = node;
