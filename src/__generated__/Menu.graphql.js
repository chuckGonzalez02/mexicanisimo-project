/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type Menu$ref: FragmentReference;
declare export opaque type Menu$fragmentType: Menu$ref;
export type Menu = $ReadOnlyArray<{|
  +id: ?string,
  +index: ?number,
  +displayName: ?string,
  +url: ?string,
  +$refType: Menu$ref,
|}>;
export type Menu$data = Menu;
export type Menu$key = $ReadOnlyArray<{
  +$data?: Menu$data,
  +$fragmentRefs: Menu$ref,
  ...
}>;
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "Menu",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "index",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "displayName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "url",
      "storageKey": null
    }
  ],
  "type": "Taxonomy"
};
// prettier-ignore
(node/*: any*/).hash = 'acd68b85ed7fb9d5fe10bc0c567fe315';

module.exports = node;
