/**
 * @flow
 * @relayHash 4da524a24df3cbdedf295323594f1b81
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AddressInput = {|
  externalNumber?: ?string,
  id?: ?string,
  internalNumber?: ?string,
  neighborhood?: ?string,
  reference?: ?string,
  street?: ?string,
  postalCode?: ?string,
  neighborhoodData?: ?NeighborhoodInput,
  selected?: ?boolean,
  place?: ?string,
  geopoint?: ?GeopointInput,
|};
export type NeighborhoodInput = {|
  id?: ?string,
  city?: ?string,
  displayname?: ?string,
  state?: ?string,
  municipality?: ?string,
|};
export type GeopointInput = {|
  lat?: ?number,
  lng?: ?number,
|};
export type Address_Upsert_MutationVariables = {|
  address?: ?AddressInput
|};
export type Address_Upsert_MutationResponse = {|
  +viewer: ?{|
    +id: ?string,
    +address: ?{|
      +id: ?string,
      +street: ?string,
      +neighborhood: ?string,
      +postalCode: ?string,
      +externalNumber: ?string,
      +internalNumber: ?string,
      +reference: ?string,
      +selected: ?boolean,
    |},
  |}
|};
export type Address_Upsert_Mutation = {|
  variables: Address_Upsert_MutationVariables,
  response: Address_Upsert_MutationResponse,
|};
*/


/*
mutation Address_Upsert_Mutation(
  $address: AddressInput
) {
  viewer {
    id
    address(address: $address) {
      id
      street
      neighborhood
      postalCode
      externalNumber
      internalNumber
      reference
      selected
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "address",
    "type": "AddressInput"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "viewer",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "address",
            "variableName": "address"
          }
        ],
        "concreteType": "Address",
        "kind": "LinkedField",
        "name": "address",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "street",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "neighborhood",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "postalCode",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "externalNumber",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "internalNumber",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "reference",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "selected",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Address_Upsert_Mutation",
    "selections": (v2/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Address_Upsert_Mutation",
    "selections": (v2/*: any*/)
  },
  "params": {
    "id": "4da524a24df3cbdedf295323594f1b81",
    "metadata": {},
    "name": "Address_Upsert_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'dd5104ccfa867b758de0aa05e0f4bfd0';

module.exports = node;
