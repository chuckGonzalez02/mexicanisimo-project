/**
 * @flow
 * @relayHash d204b0b76c26cea611717aabb96ae482
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
import type { Menu$ref } from "./Menu.graphql";
import type { Stories$ref } from "./Stories.graphql";
export type Filter = {|
  name?: ?string,
  value?: ?string,
|};
export type App_Content_QueryVariables = {|
  defaultFilter?: ?Filter
|};
export type App_Content_QueryResponse = {|
  +taxonomy: ?$ReadOnlyArray<?{|
    +$fragmentRefs: Menu$ref
  |}>,
  +$fragmentRefs: Stories$ref,
|};
export type App_Content_Query = {|
  variables: App_Content_QueryVariables,
  response: App_Content_QueryResponse,
|};
*/


/*
query App_Content_Query(
  $defaultFilter: Filter
) {
  taxonomy {
    ...Menu
    id
  }
  ...Stories
}

fragment Menu on Taxonomy {
  id
  index
  displayName
  url
}

fragment Stories on Query {
  productList(defaultFilter: $defaultFilter) {
    edges {
      node {
        ...Story
      }
      cursor
    }
  }
}

fragment Story on Product {
  id
  sku
  uri
  name
  image
  priceFormatted
  listPriceFormatted
  hasDiscount
  discountRate
  description
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "defaultFilter",
    "type": "Filter"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "App_Content_Query",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Taxonomy",
        "kind": "LinkedField",
        "name": "taxonomy",
        "plural": true,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Menu"
          }
        ],
        "storageKey": null
      },
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "Stories"
      }
    ],
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "App_Content_Query",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Taxonomy",
        "kind": "LinkedField",
        "name": "taxonomy",
        "plural": true,
        "selections": [
          (v1/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "index",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "displayName",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "url",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "defaultFilter",
            "variableName": "defaultFilter"
          }
        ],
        "concreteType": "productListConnection",
        "kind": "LinkedField",
        "name": "productList",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "productListEdge",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Product",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  (v1/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "sku",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "uri",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "name",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "image",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "priceFormatted",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "listPriceFormatted",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hasDiscount",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "discountRate",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "description",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "cursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "id": "d204b0b76c26cea611717aabb96ae482",
    "metadata": {},
    "name": "App_Content_Query",
    "operationKind": "query",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '64efcc7652f1457ec7362fbfe8679514';

module.exports = node;
