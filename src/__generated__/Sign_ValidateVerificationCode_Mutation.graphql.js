/**
 * @flow
 * @relayHash 73eaba0e70a5c7c18f7ae47a98126b20
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type Sign_ValidateVerificationCode_MutationVariables = {|
  phone?: ?string,
  verificationCode?: ?string,
|};
export type Sign_ValidateVerificationCode_MutationResponse = {|
  +validateVerificationCode: ?{|
    +id: ?string,
    +token: ?string,
  |}
|};
export type Sign_ValidateVerificationCode_Mutation = {|
  variables: Sign_ValidateVerificationCode_MutationVariables,
  response: Sign_ValidateVerificationCode_MutationResponse,
|};
*/


/*
mutation Sign_ValidateVerificationCode_Mutation(
  $phone: String
  $verificationCode: String
) {
  validateVerificationCode(phone: $phone, verificationCode: $verificationCode) {
    id
    token
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "phone",
    "type": "String"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "verificationCode",
    "type": "String"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "phone",
        "variableName": "phone"
      },
      {
        "kind": "Variable",
        "name": "verificationCode",
        "variableName": "verificationCode"
      }
    ],
    "concreteType": "Viewer",
    "kind": "LinkedField",
    "name": "validateVerificationCode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "token",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "Sign_ValidateVerificationCode_Mutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "Sign_ValidateVerificationCode_Mutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": "73eaba0e70a5c7c18f7ae47a98126b20",
    "metadata": {},
    "name": "Sign_ValidateVerificationCode_Mutation",
    "operationKind": "mutation",
    "text": null
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '0bf1af8c6f446f9d5fa38251d52bed59';

module.exports = node;
