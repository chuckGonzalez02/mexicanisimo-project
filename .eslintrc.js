module.exports = {
  env: {
    browser: true,
    node: false,
  },
  extends: ['react-app', 'airbnb', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error'],
  },
  globals: {
    process: true,
  },
};
