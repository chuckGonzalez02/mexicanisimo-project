# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.9](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.8...v0.1.9) (2020-08-03)


### Features

* flujo transaccional ([4fc4a89](https://bitbucket.org/thecorex/mexicanisimo/commit/4fc4a893a697289dae48c1200847f460b8815351))

### [0.1.8](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.7...v0.1.8) (2020-07-14)


### Bug Fixes

* se usa url web de spotify para dispositivos sin la pp ([abce843](https://bitbucket.org/thecorex/mexicanisimo/commit/abce8433bf7fa2803806715b555638ada41d49b9))

### [0.1.7](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.6...v0.1.7) (2020-07-14)

### [0.1.6](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.5...v0.1.6) (2020-07-14)


### Bug Fixes

* config ([c8dcb07](https://bitbucket.org/thecorex/mexicanisimo/commit/c8dcb071768291f672786c4b5e8834a746149778))

### [0.1.5](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.4...v0.1.5) (2020-07-14)


### Bug Fixes

* empty render ([3e76cb8](https://bitbucket.org/thecorex/mexicanisimo/commit/3e76cb8e21de6f08e51938a32c37804636b7e6ff))

### [0.1.4](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.3...v0.1.4) (2020-07-13)


### Features

* nginx proxy ([e0bfbce](https://bitbucket.org/thecorex/mexicanisimo/commit/e0bfbceb392458f3fbf95d36096fe9c390666a40))

### [0.1.3](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.2...v0.1.3) (2020-07-13)


### Bug Fixes

* git branch to master ([f91a08a](https://bitbucket.org/thecorex/mexicanisimo/commit/f91a08acd838022cb80f0c172838ed4760798270))

### [0.1.2](https://bitbucket.org/thecorex/mexicanisimo/compare/v0.1.1...v0.1.2) (2020-07-13)

### 0.1.1 (2020-07-13)


### Features

* deployment config e744cd5
* enable optional chaining 670dda6
* gcloud auth 21152dc
* mvp 1519dbf
* spotify 001ef2b
